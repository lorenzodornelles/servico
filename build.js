const build = require('./node_modules/consys/build');
const config = require('./webpack.production.config');
const package = require('./package.json');

build({
  config,
  pomPath: './pom.xml',
  package,
  packagePath: './package.json'
});