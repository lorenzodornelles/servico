package br.com.app.config.security;

import br.com.app.modulos.acesso.AcessoService;
import org.springframework.http.HttpStatus;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

public class SecurityService extends HandlerInterceptorAdapter {

    private AcessoService acessoService;

    public SecurityService(AcessoService acessoService) {
        this.acessoService = acessoService;
    }

    public static enum Verification {
        OR,
        AND
    }


    @Target({ElementType.TYPE, ElementType.METHOD})
    @Retention(RetentionPolicy.RUNTIME)
    public @interface Check {
        long[] value() default 0;
        boolean admin() default false;
//        Permissao.Acao acao() default Permissao.Acao.SELECIONAR;
        Verification verificacao() default Verification.OR;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {

        if (handler instanceof HandlerMethod) {
            HandlerMethod hand = (HandlerMethod) handler;
            String message = "Você não tem permissao para fazer isso.";
            Check check = hand.getMethodAnnotation(Check.class) != null ? hand.getMethodAnnotation(Check.class) : hand.getMethod().getDeclaringClass().getAnnotation(Check.class);
            HttpStatus httpStatus = HttpStatus.UNAUTHORIZED;
            boolean semPermissao = false;
            if (check != null) {

                if (acessoService.usuario().getCodigo() == null )
                {
                    httpStatus = HttpStatus.REQUEST_TIMEOUT;
                    message = "Devido ao tempo de inatividade, você foi desconectado";
                    semPermissao = true;
                }

                if (semPermissao == false)
                    for(long formul : check.value()) {
//                        if (!acessoService.temPermissao(formul, check.acao(), check.admin())) {
//                            semPermissao = true;
                        /*} else*/ if (check.verificacao() == Verification.OR) {
//                            se tem permissao e for do tipo OR nao precisa mais verificar,
//                            o usuario já esta apto a acessar a pagina, por isso o return
                            return true;
                        }
                    }
            }

            if (semPermissao)
            {
                response.setStatus(httpStatus.value());
                response.setContentType("text/x-json;charset=UTF-8");
                response.setHeader("Cache-Control", "no-cache");
                try {
                    PrintWriter writer = response.getWriter();
                    writer.write("{\"message\": \""+message+"\"}");
                    writer.close();
                } catch (IOException e) {
                    throw new Exception("IOException in populateWithJSON", e);
                }
            }
        }



        return true;
    }
}
