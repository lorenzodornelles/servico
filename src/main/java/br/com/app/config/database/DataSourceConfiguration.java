package br.com.app.config.database;

import org.apache.tomcat.jdbc.pool.PoolProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;

@Configuration
public class DataSourceConfiguration {

    @Autowired
    private Environment environment;

    private String dbUsername;
    private String dbPassword;
    private String dbConnection;
    private String dbLocation;
    private String dbPort;
    private String dbService;
    private String dbDriver;

    public String getDbConnection() {
        return dbConnection;
    }

    public String getDbService() {
        return dbService;
    }

    public String getDbUsername() {
        return dbUsername;
    }

    public String getDbPassword() {
        return dbPassword;
    }

    public String getDbLocation() {
        return dbLocation;
    }

    public String getDbPort() {
        return dbPort;
    }

    public String getDbUrl() {
        return dbConnection+dbLocation+":"+dbPort+"/"+dbService;
    }

    public String getDbDriver() {
        return dbDriver;
    }

    @Bean(name = "dataSource")
    @Primary
    public javax.sql.DataSource datasource() {
        this.dbConnection = "jdbc:oracle:thin:@//";
        this.dbPort = "1521";
        this.dbDriver = "oracle.jdbc.driver.OracleDriver";
        this.dbUsername = "manutencao";
        this.dbPassword = "manutencao";
        this.dbLocation = "127.0.0.1";
        this.dbService = "bdoracle";

        return datasourceConfig();
    }

    public javax.sql.DataSource datasourceConfig() {
        org.apache.tomcat.jdbc.pool.DataSource ds = new org.apache.tomcat.jdbc.pool.DataSource();
        PoolProperties p = new PoolProperties();
        p.setUrl(getDbUrl());
        p.setDriverClassName(getDbDriver());
        p.setUsername(getDbUsername());
        p.setPassword(getDbPassword());
        p.setJmxEnabled(true);
        p.setTestWhileIdle(false);
        p.setTestOnBorrow(true);
        p.setValidationQuery("SELECT 1");
        p.setTestOnReturn(false);
        p.setValidationInterval(30000);
        p.setTimeBetweenEvictionRunsMillis(30000);
        p.setMaxActive(100);
        p.setMaxIdle(100);
        p.setInitialSize(0);
        p.setMaxWait(10000);
        p.setRemoveAbandonedTimeout(60);
        p.setMinEvictableIdleTimeMillis(30000);
        p.setLogAbandoned(true);
        p.setRemoveAbandoned(true);
        p.setJdbcInterceptors(
                "org.apache.tomcat.jdbc.pool.interceptor.ConnectionState;"+
                        "org.apache.tomcat.jdbc.pool.interceptor.StatementFinalizer");
        ds.setPoolProperties(p);
        return ds;
    }
}