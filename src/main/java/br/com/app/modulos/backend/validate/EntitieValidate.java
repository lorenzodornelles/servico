package br.com.app.modulos.backend.validate;

public interface EntitieValidate<T> {
    void validate(T e) throws ValidateException;
}
