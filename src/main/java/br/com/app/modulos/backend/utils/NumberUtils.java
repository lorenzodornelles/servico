package br.com.app.modulos.backend.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class NumberUtils {

    public static Integer stringToInteger(String value) {
        Float val = stringToFloat(value);
        if (val == null)
            return null;
        return val.intValue();
    }

    public static Float stringToFloat(String value) {
        try
        {
            if (value == null)
                return null;
            value = value.replace(",", ".");
            return Float.parseFloat(value);
        }
        catch (NumberFormatException e) {
            return null;
        }
    }

    public static Double floatToDouble(Float value) {
        if (value == null)
            return null;
        return value.doubleValue();
    }


    public static BigDecimal floatToBigDecimal(Float value) {
        return floatToBigDecimal(value, null);
    }

    public static BigDecimal floatToBigDecimal(Float value, Integer decimalPlaces) {
        try
        {
            BigDecimal bigDecimal = new BigDecimal(value.toString());
            if (decimalPlaces != null)
                bigDecimal = bigDecimal.setScale(decimalPlaces, RoundingMode.CEILING);
            return bigDecimal;
        }
        catch (Exception e) {
            return null;
        }
    }
}
