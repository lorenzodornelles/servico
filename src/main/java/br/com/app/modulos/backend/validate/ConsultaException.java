package br.com.app.modulos.backend.validate;

public class ConsultaException extends DefaultException {
    public ConsultaException(String message) {
        super(message);
    }
}
