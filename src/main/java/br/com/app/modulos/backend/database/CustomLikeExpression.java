package br.com.app.modulos.backend.database;


import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.CriteriaQuery;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.engine.spi.TypedValue;
import org.hibernate.type.StringType;
import org.hibernate.type.Type;

import java.util.ArrayList;

public class CustomLikeExpression implements Criterion {

    private final String propertyName;
    private Object value;
    private MatchMode matchMode;

    public CustomLikeExpression(final String propertyName, final Object value) {
        this.propertyName = propertyName;
        this.value = value.toString().replace(",", ".");
    }

    public CustomLikeExpression(String propertyName, Object value, MatchMode matchMode) {
        this(propertyName, value);
        this.matchMode = matchMode;
    }

    public String toSqlString(final Criteria criteria, final CriteriaQuery criteriaQuery) throws HibernateException {
        final String[] columns = criteriaQuery.findColumns(this.propertyName, criteria);
        return "lower("+columns[0]+") like lower(?) ";
    }

    public TypedValue[] getTypedValues(final Criteria criteria, final CriteriaQuery criteriaQuery)
            throws HibernateException {
        final ArrayList<TypedValue> list = new ArrayList<TypedValue>();
        final Type type = criteriaQuery.getTypeUsingProjection(criteria, this.propertyName);

        Object val = "";
        if (StringType.INSTANCE.equals(type) && matchMode == null)
        {
            val = "%"+this.value.toString()+"%";
        }

        if (!StringType.INSTANCE.equals(type) || matchMode == MatchMode.START)
        {
            val = this.value.toString()+"%";
        }

        list.add(new TypedValue(StringType.INSTANCE, val));
        return list.toArray(new TypedValue[list.size()]);
    }

    @Override
    public String toString() {
        return this.propertyName + " like " + this.value;
    }
}
