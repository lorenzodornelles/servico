package br.com.app.modulos.backend.messages;

public interface MessageInterface<T> {
    void enviar(T valor);
}
