package br.com.app.modulos.backend.messages;

import org.springframework.context.i18n.LocaleContextHolder;

import java.text.MessageFormat;
import java.util.ResourceBundle;

public class MessageResource {
    public static String getMessage(String message) {
        return getMessage(message, null);
    }
    public static String getMessage(String message, Object... params)
    {
        try {
            System.out.println("CTG - LocaleContextHolder.getLocale() = " + LocaleContextHolder.getLocale());
            ResourceBundle resourceBundle = ResourceBundle.getBundle("messages/messages", LocaleContextHolder.getLocale());
            String val = message;
            if (resourceBundle != null && resourceBundle.getString(message) != null) {
                val = resourceBundle.getString(message);
                if (params != null)
                    val = MessageFormat.format(val, params);
            }
            return new String(val.getBytes("ISO-8859-1"), "UTF-8");
        }
        catch (Exception e)
        {
            return message;
        }
    }
}
