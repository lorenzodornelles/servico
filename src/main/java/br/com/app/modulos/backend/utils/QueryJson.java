package br.com.app.modulos.backend.utils;

/**
 * Created by Rafael on 18/02/2016.
 */
public class QueryJson {
    private Object data;
    private long results;

    public QueryJson() {
    }

    public QueryJson(Object data, long results) {
        this.data = data;
        this.results = results;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public long getResults() {
        return results;
    }

    public void setResults(long results) {
        this.results = results;
    }
}
