package br.com.app.modulos.backend.database;

import br.com.app.modulos.backend.utils.DefaultJsonView;
import com.fasterxml.jackson.annotation.JsonView;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.List;

public class PageJsonImpl<T> extends PageImpl<T> {
    @Override
    @JsonView(DefaultJsonView.class)
    public long getTotalElements() {
        return super.getTotalElements();
    }

    @Override
    @JsonView(DefaultJsonView.class)
    public <S> Page<S> map(Converter<? super T, ? extends S> converter) {
        return super.map(converter);
    }

    @Override
    @JsonView(DefaultJsonView.class)
    public List<T> getContent() {
        return super.getContent();
    }

    @JsonView(DefaultJsonView.class)
    @Override
    public boolean hasContent() {
        return super.hasContent();
    }


    public PageJsonImpl(List<T> content, Pageable pageable, long total) {
        super(content, pageable, total);
    }
}
