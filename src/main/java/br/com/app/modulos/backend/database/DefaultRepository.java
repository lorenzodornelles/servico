//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package br.com.app.modulos.backend.database;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

@Repository
public abstract class DefaultRepository {
    public static final Integer MIN_RESULTS = 1;
    public static final Integer MAX_RESULTS = 10;
    @PersistenceContext
    private EntityManager em;
    protected Criteria criteria;
    private DefaultRepository.Params params;
    private ArrayList<String> aliases = new ArrayList();

    public DefaultRepository() {
    }

    public abstract Class getClazz();

    public void addAlias(String alias) {
        if (!this.aliases.contains(alias) && !"pk".equals(alias) && !"id".equals(alias)) {
            this.criteria.createAlias(alias, alias, JoinType.LEFT_OUTER_JOIN);
            this.aliases.add(alias);
        }

    }

    private void checkExternalTable(String dbField) {
        if (dbField != null && dbField.contains(".")) {
            String table = dbField.split("\\.")[0];
            this.addAlias(table);
        }

    }

    public void query(DefaultRepository.Params params) {
        this.aliases = new ArrayList();
        this.params = params;
        Session session = (Session)this.em.unwrap(Session.class);
        this.criteria = session.createCriteria(this.getClazz());
        if (params != null) {
            if (params.getOrderBy() != null) {
                DefaultRepository.OrderBy[] var3 = params.getOrderBy();
                int var4 = var3.length;

                for(int var5 = 0; var5 < var4; ++var5) {
                    DefaultRepository.OrderBy ord = var3[var5];
                    this.checkExternalTable(ord.getColumn());
                    String columnName = ord.getColumn();
                    if ("desc".equals(ord.getSort())) {
                        this.criteria.addOrder(Order.desc(columnName));
                    } else {
                        this.criteria.addOrder(Order.asc(columnName));
                    }
                }
            }

            if (params.getFilters() != null && params.getFilters().size() > 0) {
                Iterator var8 = params.getFilters().entrySet().iterator();

                while(var8.hasNext()) {
                    Entry<String, DefaultRepository.Filter> entry = (Entry)var8.next();
                    DefaultRepository.Filter filter = (DefaultRepository.Filter)entry.getValue();
                    String dbField = (String)entry.getKey();
                    if (dbField != null) {
                        DefaultRepository.Type type = filter.getType();
                        this.checkExternalTable(dbField);
                        System.out.println("type BOLADA = " + type);
                        switch(type) {
                            case CONTAINS:
                                this.criteria.add(Restrictions.ilike(dbField, "%" + filter.getFilter() + "%"));
                                break;
                            case SEARCH:
                                this.criteria.add(new CustomLikeExpression(dbField, filter.getFilter()));
                                break;
                            case STARTS_WITH:
                                this.criteria.add(new CustomLikeExpression(dbField, filter.getFilter(), MatchMode.START));
                                break;
                            case ENDS_WITH:
                                this.criteria.add(Restrictions.ilike(dbField, "%" + filter.getFilter()));
                                break;
                            case EQUALS:
                                this.criteria.add(Restrictions.eq(dbField, filter.getFilter()));
                        }

                        this.criteria.addOrder(Order.asc(dbField));
                    }
                }
            }
        }

    }

    public Page search() {
        Pageable pageable = null;
        if (this.params != null && this.params.getPage() != null) {
            System.out.println(" enterHERE ");
            if (this.params.getMaxPage() == null) {
                this.params.setMaxPage(MAX_RESULTS);
            }

            this.params.setPage(this.params.getPage() - 1);
            pageable = new PageRequest(this.params.getPage(), this.params.getMaxPage());
            this.criteria.setFirstResult(pageable.getOffset());
            this.criteria.setMaxResults(pageable.getPageSize());
        }

        List lista = this.criteria.list();
        this.criteria.setFirstResult(0);
        this.criteria.setMaxResults(1);
        Long results = (Long)this.criteria.setProjection(Projections.rowCount()).uniqueResult();
        if (pageable == null) {
            pageable = new PageRequest(0, results.intValue() < 1 ? 1 : results.intValue());
        }

        return new PageJsonImpl(lista, pageable, results);
    }

    public static class Params {
        private DefaultRepository.OrderBy[] orderBy;
        private String groupBy;
        private Integer page;
        private Integer maxPage;
        private Integer maxResults;
        private Map<String, DefaultRepository.Filter> filters = new HashMap();

        public Params() {
        }

        public DefaultRepository.OrderBy[] getOrderBy() {
            return this.orderBy;
        }

        public void setOrderBy(DefaultRepository.OrderBy[] orderBy) {
            this.orderBy = orderBy;
        }

        public String getGroupBy() {
            return this.groupBy;
        }

        public void setGroupBy(String groupBy) {
            this.groupBy = groupBy;
        }

        public Integer getPage() {
            return this.page;
        }

        public void setPage(Integer page) {
            this.page = page;
        }

        public Integer getMaxPage() {
            return this.maxPage;
        }

        public void setMaxPage(Integer maxPage) {
            this.maxPage = maxPage;
        }

        public Integer getMaxResults() {
            return this.maxResults;
        }

        public void setMaxResults(Integer maxResults) {
            this.maxResults = maxResults;
        }

        public Map<String, DefaultRepository.Filter> getFilters() {
            return this.filters;
        }

        public void setFilters(Map<String, DefaultRepository.Filter> filters) {
            this.filters = filters;
        }

        public String toString() {
            return "Params{orderBy=" + Arrays.toString(this.orderBy) + ", groupBy='" + this.groupBy + '\'' + ", page=" + this.page + ", maxPage=" + this.maxPage + ", maxResults=" + this.maxResults + ", filters=" + this.filters + '}';
        }
    }

    public static class OrderBy {
        @JsonProperty("field")
        private String column;
        @JsonProperty("sort")
        private String sort;

        public OrderBy() {
        }

        public String getColumn() {
            return this.column;
        }

        public void setColumn(String column) {
            this.column = column;
        }

        public String getSort() {
            return this.sort;
        }

        public void setSort(String sort) {
            this.sort = sort;
        }
    }

    public static class Filter {
        @JsonProperty("filter")
        private Object filter;
        private DefaultRepository.Type type;

        public Filter() {
        }

        public Filter(Object filter) {
            this.filter = filter;
            this.type = DefaultRepository.Type.SEARCH;
        }

        public Filter(Object filter, DefaultRepository.Type type) {
            this.filter = filter;
            this.type = type;
        }

        public Object getFilter() {
            if (this.filter instanceof String) {
                this.filter = this.filter.toString().toLowerCase();
            }

            return this.filter;
        }

        public void setFilter(Object filter) {
            this.filter = filter;
        }

        public DefaultRepository.Type getType() {
            return this.type;
        }

        @JsonProperty("type")
        public void setType(Integer type) {
            if (type == null) {
                this.type = DefaultRepository.Type.SEARCH;
            } else {
                this.type = DefaultRepository.Type.from(type);
            }
        }

        public void setType(DefaultRepository.Type type) {
            this.type = type;
        }
    }

    public static enum Type {
        CONTAINS(1),
        EQUALS(2),
        STARTS_WITH(3),
        ENDS_WITH(4),
        SEARCH(5);

        private int type;

        private Type(int type) {
            this.type = type;
        }

        public int getType() {
            return this.type;
        }

        public static DefaultRepository.Type from(int type) {
            DefaultRepository.Type[] var1 = values();
            int var2 = var1.length;

            for(int var3 = 0; var3 < var2; ++var3) {
                DefaultRepository.Type tp = var1[var3];
                if (tp.getType() == type) {
                    return tp;
                }
            }

            return null;
        }
    }
}
