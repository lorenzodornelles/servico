package br.com.app.modulos.backend.validate;

public class ValidateException extends DefaultException {
    public ValidateException(String message, Object... params) {
        super(message, params);
    }

    public ValidateException(String message) {
        super(message);
    }
}
