package br.com.app.modulos.backend.messages;

public enum MessageType {
    SUCCESS, INFO, WARNING, ERROR
}
