//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package br.com.app.modulos.backend.cookie;

import br.com.app.modulos.backend.access.LoggedUser;
import java.io.Serializable;
import java.util.Date;

public class CookieInfo implements Serializable {
  private static final long serialVersionUID = -1L;
  private LoggedUser loggedUser;
  private Date expirationDate;

  public CookieInfo(LoggedUser loggedUser, Date expirationDate) {
    this.loggedUser = loggedUser;
    this.expirationDate = expirationDate;
  }

  public LoggedUser getLoggedUser() {
    return this.loggedUser;
  }

  public void setLoggedUser(LoggedUser loggedUser) {
    this.loggedUser = loggedUser;
  }

  public Date getExpirationDate() {
    return this.expirationDate;
  }

  public void setExpirationDate(Date expirationDate) {
    this.expirationDate = expirationDate;
  }
}
