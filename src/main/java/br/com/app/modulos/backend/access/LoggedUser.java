package br.com.app.modulos.backend.access;

import java.util.Locale;

public interface LoggedUser {
    Locale getLocale();

    void setToken(String var1);
}
