package br.com.app.modulos.backend.utils;

public class FuncaoDoBancoException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public FuncaoDoBancoException(String msg) {
        super(msg);
    }

    public FuncaoDoBancoException(Exception e) {
        super(e);
    }

}
