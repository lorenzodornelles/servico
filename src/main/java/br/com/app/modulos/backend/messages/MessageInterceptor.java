package br.com.app.modulos.backend.messages;

import br.com.app.modulos.backend.access.AccessService;
import br.com.app.modulos.backend.access.LoggedUser;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class MessageInterceptor extends HandlerInterceptorAdapter {
    private AccessService acessoService;

    public MessageInterceptor(AccessService acessoService) {
        this.acessoService = acessoService;
    }

    public boolean preHandle(HttpServletRequest request,
                             HttpServletResponse response, Object handler) throws Exception {
        LoggedUser usuarioLogado  = acessoService.usuario();
        LocaleContextHolder.setLocale(usuarioLogado.getLocale());
        return true;
    }
}
