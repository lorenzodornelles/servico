package br.com.app.modulos.backend.validate;

import br.com.app.modulos.backend.messages.MessageResource;

public class DefaultException extends Exception {

    private String message;
    private Object[] params;

    public DefaultException() {
    }

    public DefaultException(String message) {
        this(message, null);
    }

    public DefaultException(String message, Object... params) {
        super(message);
        this.message = message;
        this.params = params;
    }

    @Override
    public String getMessage() {
        return MessageResource.getMessage(message, params);
    }
}
