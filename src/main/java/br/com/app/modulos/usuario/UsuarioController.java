package br.com.app.modulos.usuario;

import br.com.app.modulos.backend.database.DefaultRepository;
import br.com.app.modulos.backend.validate.ValidateException;
import com.fasterxml.jackson.annotation.JsonView;
import br.com.app.modulos.backend.api.ApiController;
import br.com.app.modulos.filtro.FiltroPadrao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureOrder;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;

import javax.xml.bind.ValidationException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.List;

@RestController
public class UsuarioController extends ApiController {

    @Autowired
    private UsuarioRepositoryFinder usuarioRepositoryFinder;
    @Autowired
    private UsuarioRepository usuarioRepository;

    @JsonView({Usuario.Lista.class})
    @RequestMapping(value = "/usuario/consulta", method = RequestMethod.POST)
    public Page<Usuario> consultarUsuarios(@RequestBody FiltroPadrao filtro) {
        return usuarioRepositoryFinder.getUsuarios(filtro.getDescricao(), new PageRequest(filtro.getPagina(), 10));
    }

    @RequestMapping(value = "/usuario", method = RequestMethod.DELETE)
    public boolean excluirUsuario(@RequestParam(value="codigo", defaultValue="0") Integer codigo) throws ValidateException {
        try {
            usuarioRepositoryFinder.delete(codigo);
        }catch (DataIntegrityViolationException e){
            if (e.getRootCause() instanceof SQLIntegrityConstraintViolationException) {
                SQLIntegrityConstraintViolationException sql = (SQLIntegrityConstraintViolationException) e.getRootCause();
                int error = sql.getErrorCode();
                if(error == 2392) {
                    throw new ValidateException("Este registro está sendo usado em outro cadastro");
                }
            }
        }
        return true;
    }

    @JsonView(Usuario.Cadastro.class)
    @RequestMapping(value = "/usuario", method = RequestMethod.GET)
    public Usuario getUsuario(@RequestParam(value = "codigo", defaultValue = "0") Integer codigo) {
        return usuarioRepositoryFinder.findOne(codigo);
    }

    @JsonView(Usuario.Lista.class)
    @RequestMapping(value = "/usuario", method = RequestMethod.POST)
    public Usuario salvarUsuario(@RequestBody Usuario usuario) throws ValidationException {
        if ((usuario.getCodigo() == null || usuario.getCodigo() == 0) && usuarioRepositoryFinder.countUsuarioLogin(usuario.getLogin()) > 0) {
            throw new ValidationException("O login informado já está cadastrado");
        }

        return usuarioRepositoryFinder.save(usuario);
    }

    @JsonView(Usuario.Typeahead.class)
    @RequestMapping(value = "/usuario/typeahead", method = RequestMethod.POST)
    public @ResponseBody
    List<Usuario> typeaheadUsuario(@RequestBody(required = false) FiltroPadrao filtro) {
        return usuarioRepositoryFinder.getUsuariosList(filtro.getDescricao(), new PageRequest(0, 5));
    }

    @JsonView(Usuario.Typeahead.class)
    @RequestMapping(value = "/usuario/lookup", method = RequestMethod.POST)
    public Page<Usuario> lookupUsuario(@RequestBody DefaultRepository.Params params) {
        return usuarioRepository.find(params);
    }
}
