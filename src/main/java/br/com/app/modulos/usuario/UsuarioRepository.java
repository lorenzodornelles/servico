package br.com.app.modulos.usuario;

import br.com.app.modulos.acesso.AcessoException;
import br.com.app.modulos.backend.database.DefaultRepository;
import org.hibernate.criterion.Restrictions;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Repository
public class UsuarioRepository extends DefaultRepository {
    @Override
    public Class getClazz() { return Usuario.class; }

    @PersistenceContext
    private EntityManager em;

    public Page<Usuario> find(Params params) {
        query(params);
        criteria.add(Restrictions.eq("ativo", "S"));
        return search();
    }

    public Usuario isAutorizado(String login, String senha) throws AcessoException {
        try {
            if (login == null || senha == null)
                return null;
            String sql =
                    " SELECT new br.com.app.modulos.usuario.Usuario(u.codigo, u.login, u.nome, u.ativo, u.admin)" +
                    " FROM Usuario u" +
                    " WHERE" +
                    " UPPER(u.login) = :login"+
                    " AND u.senha = :senha";

            Query query = em.createQuery(sql)
                    .setParameter("login", login.toUpperCase())
                    .setParameter("senha", senha);
            Usuario usuario = (Usuario) query.getSingleResult();
            if (usuario.getAtivo().equals("N"))
                throw new AcessoException("error.usuario.inativo");
            return usuario;
        }
        catch (NoResultException e)
        {
            return null;
        }
    }
}
