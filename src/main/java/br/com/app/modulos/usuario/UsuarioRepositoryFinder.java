package br.com.app.modulos.usuario;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface UsuarioRepositoryFinder extends CrudRepository<Usuario, Integer> {

    @Query("from Usuario u where (:descricao is null or to_char(u.codigo) = :descricao or " +
            "lower(u.login) like lower('%'||:descricao||'%') or " +
            "lower(u.nome) like lower('%'||:descricao||'%')) " +
            "order by u.codigo asc")
    Page<Usuario> getUsuarios(@Param("descricao") String descricao, Pageable pageable);

    @Query("select count(u.codigo) from Usuario u where lower(:login) = (u.login)")
    Integer countUsuarioLogin(@Param("login") String login);

    @Query("select distinct new br.com.app.modulos.usuario.Usuario(u.codigo, u.nome) " +
            "from Usuario u where (:descricao = null or (to_char(u.codigo) = :descricao or " +
            "lower(u.nome) like lower('%'||:descricao||'%'))) " +
            "order by u.codigo asc")
    List<Usuario> getUsuariosList(@Param("descricao") String descricao, Pageable pageable);
}
