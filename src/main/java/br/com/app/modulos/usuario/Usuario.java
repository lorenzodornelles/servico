package br.com.app.modulos.usuario;

import br.com.app.modulos.servico.Servico;
import com.fasterxml.jackson.annotation.JsonView;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "SIUSUARI")
public class Usuario {
    public interface Lista {}
    public interface Cadastro extends Lista {}
    public interface Typeahead {}

    public Usuario() {
    }

    public Usuario(Integer codigo, String login, String nome, String ativo, String admin) {
        this.codigo = codigo;
        this.login = login;
        this.nome = nome;
        this.ativo = ativo;
        this.admin = admin;
    }

    public Usuario(Integer codigo, String nome) {
        this.codigo = codigo;
        this.nome = nome;
    }

    @Id
    @JsonView({Lista.class, Servico.Lista.class, Typeahead.class})
    @Column(name = "CD_USUARI")
    private Integer codigo;

    @JsonView(Cadastro.class)
    @Column(name = "DS_LOGIN")
    private String login;

    @JsonView({Lista.class, Servico.Lista.class, Typeahead.class})
    @Column(name = "DS_NOME")
    private String nome;

    @JsonView(Cadastro.class)
    @Column(name = "DS_SENHA")
    private String senha;

    @JsonView(Lista.class)
    @Column(name = "CH_ATIVO")
    private String ativo;

    @JsonView(Lista.class)
    @Column(name = "CH_ADM")
    private String admin;

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getAtivo() {
        return ativo;
    }

    public void setAtivo(String ativo) {
        this.ativo = ativo;
    }

    public String getAdmin() {
        return admin;
    }

    public void setAdmin(String admin) {
        this.admin = admin;
    }
}