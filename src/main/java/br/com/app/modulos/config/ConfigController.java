package br.com.app.modulos.config;

import br.com.app.modulos.backend.responses.GenericResponse;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class ConfigController {

    private static final Logger logger = Logger.getLogger(ConfigController.class);

    @Autowired
    private ConfigService configService;

    @RequestMapping(value = "/api/versao", method = RequestMethod.GET)
    public @ResponseBody GenericResponse versao() {
        return new GenericResponse(configService.getApplicationPomVersion());
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String index() throws Exception {
        return "index.html";
    }
}
