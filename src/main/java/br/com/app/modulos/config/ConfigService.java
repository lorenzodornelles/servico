package br.com.app.modulos.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Date;

@Service
@PropertySource(value = "classpath:META-INF/maven/br.com.app/servico/pom.properties", ignoreResourceNotFound=true)
public class ConfigService {
    private String applicationPomVersion;

    @PersistenceContext
    private EntityManager em;

    @Autowired
    public ConfigService(@Value("${version:dev}") String applicationVersion ) {
        this.applicationPomVersion = applicationVersion;
    }

    public Date getDatabaseDate() {
        try {
            return (Date) em.createNativeQuery("SELECT SYSDATE FROM DUAL").getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new Date();
    }

    public String getApplicationPomVersion() {
        return this.applicationPomVersion;
    }
}
