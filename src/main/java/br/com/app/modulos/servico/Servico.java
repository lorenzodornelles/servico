package br.com.app.modulos.servico;

import br.com.app.modulos.cliente.Cliente;
import br.com.app.modulos.usuario.Usuario;
import com.fasterxml.jackson.annotation.JsonView;
import javax.persistence.Id;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "SISERVICO")
public class Servico {
    public interface Lista {}
    public interface Cadastro extends Lista {}
    public interface Default extends Lista, Cadastro {}

    @Id
    @JsonView(Lista.class)
    @Column(name = "CD_SERVICO")
    private Integer codigo;

    @ManyToOne
    @JsonView(Lista.class)
    @JoinColumn(name = "CD_CLIENT")
    private Cliente cliente;

    @JsonView(Cadastro.class)
    @Column(name = "DS_PROBLE")
    private String problema;

    @JsonView(Lista.class)
    @Column(name = "DS_TIPO")
    private String tipo;

    @JsonView(Lista.class)
    @Column(name = "DS_MARCA")
    private String marca;

    @JsonView(Lista.class)
    @Column(name = "CH_STATUS")
    private String status;

    @JsonView(Cadastro.class)
    @Column(name = "DS_MOTIVO")
    private String motivo;

    @JsonView(Lista.class)
    @Column(name = "DT_INICIO")
    private Date inicio;

    @JsonView(Lista.class)
    @Column(name = "DT_FIM")
    private Date fim;

    @ManyToOne
    @JsonView(Lista.class)
    @JoinColumn(name = "CD_USUARI")
    private Usuario encarregado;

    @JsonView(Lista.class)
    @Column(name = "DT_CADAST")
    private Date cadastro;

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public String getProblema() {
        return problema;
    }

    public void setProblema(String problema) {
        this.problema = problema;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public Date getInicio() {
        return inicio;
    }

    public void setInicio(Date inicio) {
        this.inicio = inicio;
    }

    public Date getFim() {
        return fim;
    }

    public void setFim(Date fim) {
        this.fim = fim;
    }

    public Usuario getEncarregado() {
        return encarregado;
    }

    public void setEncarregado(Usuario encarregado) {
        this.encarregado = encarregado;
    }

    public Date getCadastro() {
        return cadastro;
    }

    public void setCadastro(Date cadastro) {
        this.cadastro = cadastro;
    }
}
