package br.com.app.modulos.servico;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ServicoRepositoryFinder extends CrudRepository<Servico, Integer> {

    @Query("from Servico s where (:descricao is null or to_char(s.codigo) = :descricao or " +
            "lower(s.cliente.nome) like lower('%'||:descricao||'%') or " +
            "lower(s.tipo) like lower('%'||:descricao||'%') or " +
            "lower(s.marca) like lower('%'||:descricao||'%')) and " +
            "(:status = 'T' or s.status = :status) " +
            "order by s.cadastro desc")
    Page<Servico> getServicos(@Param("descricao") String descricao,
                              @Param("status") String status,
                              Pageable pageable);

    @Query("from Servico s where (s.status = 'P' or s.status = 'I') and " +
           "s.encarregado.codigo = :codigo " +
           "order by s.cadastro desc")
    List<Servico> getMeusServicoPendentes(@Param("codigo") Integer codigo);
}
