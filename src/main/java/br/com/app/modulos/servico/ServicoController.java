package br.com.app.modulos.servico;

import br.com.app.modulos.acesso.AcessoService;
import br.com.app.modulos.backend.api.ApiController;
import br.com.app.modulos.backend.validate.ValidateException;
import br.com.app.modulos.filtro.FiltroServico;
import com.fasterxml.jackson.annotation.JsonView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;

import javax.xml.bind.ValidationException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.Date;
import java.util.List;

@RestController
public class ServicoController extends ApiController {

    @Autowired
    private ServicoRepositoryFinder servicoRepositoryFinder;
    @Autowired
    private AcessoService acessoService;

    @JsonView({Servico.Lista.class})
    @RequestMapping(value = "/servico/consulta", method = RequestMethod.POST)
    public Page<Servico> consultarServicos(@RequestBody FiltroServico filtro) {
        return servicoRepositoryFinder.getServicos(filtro.getDescricao(),
                                                   filtro.getStatus(),
                                                   new PageRequest(filtro.getPagina(), 10));
    }

    @RequestMapping(value = "/servico", method = RequestMethod.DELETE)
    public boolean excluirServico(@RequestParam(value="codigo", defaultValue="0") Integer codigo) throws ValidateException {
        try {
            servicoRepositoryFinder.delete(codigo);
        }catch (DataIntegrityViolationException e){
            if (e.getRootCause() instanceof SQLIntegrityConstraintViolationException) {
                SQLIntegrityConstraintViolationException sql = (SQLIntegrityConstraintViolationException) e.getRootCause();
                int error = sql.getErrorCode();
                if(error == 2392) {
                    throw new ValidateException("Este registro está sendo usado em outro cadastro");
                }
            }
        }
        return true;
    }

    @JsonView({Servico.Default.class})
    @RequestMapping(value = "/servico/meus_pendentes", method = RequestMethod.GET)
    public List<Servico> consultarMeusPendentes() {
        return servicoRepositoryFinder.getMeusServicoPendentes(acessoService.usuario().getCodigo());
    }

    @JsonView(Servico.Lista.class)
    @RequestMapping(value = "/servico", method = RequestMethod.POST)
    public Servico salvarServico(@RequestBody Servico servico) {
        if (servico.getCodigo() == null || servico.getCodigo() == 0) {
            servico.setCadastro(new Date());
            servico.setInicio(null);
            servico.setStatus("P");
        }

        return servicoRepositoryFinder.save(servico);
    }

    @JsonView(Servico.Default.class)
    @RequestMapping(value = "/servico", method = RequestMethod.GET)
    public Servico getServico(@RequestParam(value = "codigo", defaultValue = "0") Integer codigo) {
        return servicoRepositoryFinder.findOne(codigo);
    }
}
