package br.com.app.modulos.cliente;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ClienteRepositoryFinder extends CrudRepository<Cliente, Integer> {

    @Query("from Cliente c where (:descricao is null or to_char(c.codigo) = :descricao or " +
            "lower(c.nome) like lower('%'||:descricao||'%') or " +
            "lower(c.email) like lower('%'||:descricao||'%')) " +
            "order by c.codigo asc")
    Page<Cliente> getClientes(@Param("descricao") String descricao, Pageable pageable);

    @Query("from Cliente c order by c.codigo desc")
    List<Cliente> getClientesRecentes(Pageable pageable);

    @Query("select count(u.codigo) from Cliente u where :documento = u.documento")
    Integer countClienteDocumento(@Param("documento") String documento);

    @Query("select distinct new br.com.app.modulos.cliente.Cliente(c.codigo, c.nome) " +
            "from Cliente c where (:descricao = null or (to_char(c.codigo) = :descricao or " +
            "lower(c.nome) like lower('%'||:descricao||'%'))) " +
            "order by c.codigo asc")
    List<Cliente> getClientesList(@Param("descricao") String descricao, Pageable pageable);
}
