package br.com.app.modulos.cliente;

import br.com.app.modulos.backend.database.DefaultRepository;
import br.com.app.modulos.backend.validate.ValidateException;
import com.fasterxml.jackson.annotation.JsonView;
import br.com.app.modulos.backend.api.ApiController;
import br.com.app.modulos.filtro.FiltroPadrao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;

import javax.xml.bind.ValidationException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.List;

@RestController
public class ClienteController extends ApiController {

    @Autowired
    private ClienteRepositoryFinder clienteRepositoryFinder;
    @Autowired
    private ClienteRepository clienteRepository;

    @JsonView({Cliente.Lista.class})
    @RequestMapping(value = "/cliente/consulta", method = RequestMethod.POST)
    public Page<Cliente> consultarClientes(@RequestBody FiltroPadrao filtro) {
        return clienteRepositoryFinder.getClientes(filtro.getDescricao(), new PageRequest(filtro.getPagina(), 10));
    }

    @JsonView(Cliente.Cadastro.class)
    @RequestMapping(value = "/cliente", method = RequestMethod.GET)
    public Cliente getCliente(@RequestParam(value = "codigo", defaultValue = "0") Integer codigo) {
        return clienteRepositoryFinder.findOne(codigo);
    }

    @RequestMapping(value = "/cliente", method = RequestMethod.DELETE)
    public boolean excluirCliente(@RequestParam(value="codigo", defaultValue="0") Integer codigo) throws ValidateException {
        try {
            clienteRepositoryFinder.delete(codigo);
        }catch (DataIntegrityViolationException e){
            if (e.getRootCause() instanceof SQLIntegrityConstraintViolationException) {
                SQLIntegrityConstraintViolationException sql = (SQLIntegrityConstraintViolationException) e.getRootCause();
                int error = sql.getErrorCode();
                if(error == 2392) {
                    throw new ValidateException("Este registro está sendo usado em outro cadastro");
                }
            }
        }
        return true;
    }

    @JsonView(Cliente.Lista.class)
    @RequestMapping(value = "/cliente", method = RequestMethod.POST)
    public Cliente salvarCliente(@RequestBody Cliente cliente) throws ValidationException {
        if ((cliente.getCodigo() == null || cliente.getCodigo() == 0) && clienteRepositoryFinder.countClienteDocumento(cliente.getDocumento()) > 0) {
            throw new ValidationException("O "+(cliente.getPessoa().equals("F") ? "CPF" : "CNPJ")+" informado já está cadastrado");
        }

        return clienteRepositoryFinder.save(cliente);
    }

    @JsonView({Cliente.Lista.class})
    @RequestMapping(value = "/cliente/recentes", method = RequestMethod.GET)
    public List<Cliente> clientesRecentes() {
        return clienteRepositoryFinder.getClientesRecentes(new PageRequest(0, 5));
    }

    @JsonView(Cliente.Typeahead.class)
    @RequestMapping(value = "/cliente/typeahead", method = RequestMethod.POST)
    public @ResponseBody List<Cliente> typeaheadCliente(@RequestBody(required = false) FiltroPadrao filtro) {
        return clienteRepositoryFinder.getClientesList(filtro.getDescricao(), new PageRequest(0, 5));
    }

    @JsonView(Cliente.Typeahead.class)
    @RequestMapping(value = "/cliente/lookup", method = RequestMethod.POST)
    public Page<Cliente> lookupCliente(@RequestBody DefaultRepository.Params params) {
        return clienteRepository.find(params);
    }
}
