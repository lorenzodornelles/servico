package br.com.app.modulos.cliente;

import br.com.app.modulos.servico.Servico;
import com.fasterxml.jackson.annotation.JsonView;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "SICLIENT")
public class Cliente {
    public interface Lista {}
    public interface Cadastro extends Lista {}
    public interface Typeahead {}

    public Cliente() {
    }

    public Cliente(Integer codigo, String nome) {
        this.codigo = codigo;
        this.nome = nome;
    }

    @Id
    @JsonView({Lista.class, Servico.Lista.class, Typeahead.class})
    @Column(name = "CD_CLIENT")
    private Integer codigo;

    @JsonView({Lista.class, Servico.Lista.class, Typeahead.class})
    @Column(name = "DS_CLIENT")
    private String nome;

    @JsonView(Lista.class)
    @Column(name = "DS_EMAIL")
    private String email;

    @JsonView(Lista.class)
    @Column(name = "NR_FONE")
    private String telefone;

    @JsonView(Lista.class)
    @Column(name = "CH_PESSOA")
    private String pessoa;

    @JsonView({Lista.class, Typeahead.class})
    @Column(name = "DS_DOC")
    private String documento;

    @JsonView(Cadastro.class)
    @Column(name = "CH_CEP")
    private String cep;

    @JsonView(Cadastro.class)
    @Column(name = "DS_RUA")
    private String rua;

    @JsonView(Cadastro.class)
    @Column(name = "DS_BAIRRO")
    private String bairro;

    @JsonView(Cadastro.class)
    @Column(name = "DS_CIDADE")
    private String cidade;

    @JsonView(Cadastro.class)
    @Column(name = "CH_ESTADO")
    private String estado;

    @JsonView(Cadastro.class)
    @Column(name = "NR_NUMERO")
    private Integer numero;

    @JsonView(Cadastro.class)
    @Column(name = "DS_COMPLE")
    private String complemento;

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getPessoa() {
        return pessoa;
    }

    public void setPessoa(String pessoa) {
        this.pessoa = pessoa;
    }

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getRua() {
        return rua;
    }

    public void setRua(String rua) {
        this.rua = rua;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Integer getNumero() {
        return numero;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }
}
