package br.com.app.modulos.cliente;

import br.com.app.modulos.backend.database.DefaultRepository;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Subqueries;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Repository;

import javax.xml.bind.ValidationException;

@Repository
public class ClienteRepository extends DefaultRepository {
    @Override
    public Class getClazz() {
        return Cliente.class;
    }

    public Page<Cliente> find(Params params) {
        query(params);

        DetachedCriteria userSubquery = DetachedCriteria.forClass(Cliente.class, "c").setProjection(Projections.property("c.codigo"));
        criteria.add(Subqueries.propertyIn("codigo", userSubquery))
                .addOrder(Order.asc("codigo"));
        return search();
    }
}
