package br.com.app.modulos.filtro;

public class FiltroServico extends FiltroPadrao {
    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "FiltroServico{" +
                "status='" + status + '\'' +
                '}';
    }
}
