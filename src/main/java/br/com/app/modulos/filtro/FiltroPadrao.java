package br.com.app.modulos.filtro;

public class FiltroPadrao {
    private String descricao;
    private Integer pagina;

    public String getDescricao() {
        if (descricao != null && descricao.trim().isEmpty())
            return null;
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Integer getPagina() {
        if(pagina != null)
            return pagina -1;
        return pagina;
    }

    public void setPagina(Integer pagina) {
        this.pagina = pagina;
    }

    @Override
    public String toString() {
        return "FiltroPadrao{" +
                "descricao='" + descricao + '\'' +
                ", pagina=" + pagina +
                '}';
    }
}
