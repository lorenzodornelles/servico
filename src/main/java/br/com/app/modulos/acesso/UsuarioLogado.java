package br.com.app.modulos.acesso;

import br.com.app.modulos.backend.access.LoggedUser;
import br.com.app.modulos.backend.utils.DefaultJsonView;
import com.fasterxml.jackson.annotation.JsonView;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.io.Serializable;
import java.util.Collection;
import java.util.Locale;

public class UsuarioLogado implements UserDetails, Serializable, LoggedUser {
    private static final long serialVersionUID = -1;
    public interface Padrao extends DefaultJsonView {}
    public static UsuarioLogado DESLOGADO = new UsuarioLogado();

    public UsuarioLogado() {
    }

    @JsonView(Padrao.class)
    private Integer codigo;

    @JsonView(Padrao.class)
    private String descricao;

    @JsonView(Padrao.class)
    private Locale locale;

    @JsonView(Padrao.class)
    private String token;

    @JsonView(Padrao.class)
    private boolean admin;

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public Locale getLocale() {
        return locale;
    }

    public String getToken() {
        return token;
    }

    @Override
    public void setToken(String token) {
        this.token = token;
    }

    public void setLocale(Locale locale) {
        this.locale = locale;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    @Override
    public String getPassword() {
        return null;
    }

    @Override
    public String getUsername() {
        return descricao;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
