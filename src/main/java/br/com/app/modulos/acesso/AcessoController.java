package br.com.app.modulos.acesso;

import br.com.app.modulos.backend.api.ApiController;
import br.com.app.modulos.usuario.Usuario;
import br.com.app.modulos.backend.validate.ValidateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;

@RestController
public class AcessoController extends ApiController {

    @Autowired
    private AcessoService acessoService;

    @RequestMapping(value = "/entrar", method = RequestMethod.POST)
    public @ResponseBody UsuarioLogado entrar(@Validated @RequestBody Usuario user,
                                              HttpServletResponse httpServletResponse) throws AcessoException, ValidateException {
        return acessoService.logar(httpServletResponse, user.getLogin(), user.getSenha(), true);
    }

    @RequestMapping(value = "/sair", method = RequestMethod.GET)
    public @ResponseBody boolean sair(HttpServletResponse httpServletResponse) throws AcessoException, ValidateException {
        acessoService.deslogar(httpServletResponse);
        return true;
    }
}


