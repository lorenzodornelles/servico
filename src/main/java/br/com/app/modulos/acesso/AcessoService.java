package br.com.app.modulos.acesso;

import br.com.app.modulos.backend.access.AccessService;
import br.com.app.modulos.backend.cookie.CookieInfo;
import br.com.app.modulos.usuario.Usuario;
import br.com.app.modulos.usuario.UsuarioRepository;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Service
@Configuration
public class AcessoService implements AccessService {
    private static String AUTHENTICATION_HEADER = "Authentication";
    private static String COOKIE_FILE = "./cookies.ser";

    @Autowired
    private HttpServletRequest httpServletRequest;

    @Autowired
    private UsuarioRepository usuarioRepository;

    private Map<String, CookieInfo> cookies = new HashMap<>();

    private SecureRandom random = new SecureRandom();

    public UsuarioLogado logar(HttpServletResponse httpServletResponse, String login, String senha) throws AcessoException {
        return logar(httpServletResponse, login, senha, false);
    }
    public UsuarioLogado logar(HttpServletResponse httpServletResponse, String login, String senha, boolean lembrarme) throws AcessoException {
        UsuarioLogado usuarioLogado = new UsuarioLogado();

        Usuario usuario = usuarioRepository.isAutorizado(login, senha);
        if (usuario == null)
            throw new AcessoException("error.login.senha");

        usuarioLogado.setCodigo(usuario.getCodigo());
        usuarioLogado.setDescricao(usuario.getNome());
        usuarioLogado.setAdmin(usuario.getAdmin().equals("S"));

        Map<String, CookieInfo> cookies = this.getCookies();
        String hash = new BigInteger(130, random).toString(32).toUpperCase();
        DateTime dataExpira = DateTime.now();
        dataExpira = dataExpira.plusDays(1);
        if (lembrarme)
            cookies.put(hash, new CookieInfo(usuarioLogado, null));
        else
            cookies.put(hash, new CookieInfo(usuarioLogado, dataExpira.toDate()));
        usuarioLogado.setToken(hash);
        setCookies(cookies);

        return usuarioLogado;
    }

    public boolean deslogar(HttpServletResponse httpServletResponse) {
        Map<String, CookieInfo> cookies = this.getCookies();

        String hash = httpServletRequest.getHeader(AUTHENTICATION_HEADER);
        cookies.remove(hash);
        setCookies(cookies);
        return true;
    }
    public void usuario(UsuarioLogado usuarioLogado) {
        String hash = httpServletRequest.getHeader(AUTHENTICATION_HEADER);
        Map<String, CookieInfo> cookies = this.getCookies();
        CookieInfo cookieInfo = cookies.get(hash);
        cookieInfo.setLoggedUser(usuarioLogado);
        this.setCookies(cookies);
    }

    public UsuarioLogado usuario() {
        String hash = httpServletRequest.getHeader(AUTHENTICATION_HEADER);
        Map<String, CookieInfo> cookies = this.getCookies();
        CookieInfo cookieInfo = cookies.get(hash);
        if (cookieInfo == null)
            return UsuarioLogado.DESLOGADO;
        Date dataAtual = new Date();
        if (cookieInfo.getExpirationDate() != null && dataAtual.after(cookieInfo.getExpirationDate()))
            return UsuarioLogado.DESLOGADO;
        UsuarioLogado usuarioLogado = (UsuarioLogado) cookieInfo.getLoggedUser();
        if (usuarioLogado == null)
            return UsuarioLogado.DESLOGADO;
        return usuarioLogado;
    }

    public void setCookies(Map<String, CookieInfo> cookies) {
        cookies(cookies);
    }

    public synchronized Map<String, CookieInfo> getCookies() {
        return cookies(null);
    }

    private synchronized Map<String, CookieInfo> cookies(Map<String, CookieInfo> cookies) {
        try
        {
            if (cookies == null)
            {
                if (!this.cookies.isEmpty())
                    return this.cookies;
                FileInputStream fileIn = new FileInputStream(COOKIE_FILE);
                ObjectInputStream in = new ObjectInputStream(fileIn);
                this.cookies = (Map<String, CookieInfo>) in.readObject();
                in.close();
                fileIn.close();
            }
            else {
                FileOutputStream fileOut = new FileOutputStream(COOKIE_FILE);
                ObjectOutputStream out = new ObjectOutputStream(fileOut);
                out.writeObject(cookies);
                out.close();
                fileOut.close();
                this.cookies = cookies;

            }
        }
        catch (FileNotFoundException e)
        {
        }
        catch (EOFException e)
        {
            e.printStackTrace();
        }
        catch(IOException e) {
            e.printStackTrace();
        }
        catch (ClassNotFoundException e)
        {
            e.printStackTrace();
        }
        return this.cookies;
    }
}
