package br.com.app.modulos.acesso;

import br.com.app.modulos.backend.validate.DefaultException;

public class AcessoException extends DefaultException {
    public AcessoException(String message) {
        super(message);
    }
}
