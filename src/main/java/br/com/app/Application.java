package br.com.app;

import org.apache.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;

import java.util.Date;

@SpringBootApplication()
public class Application {

    public static final Logger LOGGER = Logger.getLogger(Application.class);
    public static final Class CLAZZ = Application.class;

    @Bean(name = "messageSource")
    public ReloadableResourceBundleMessageSource messageSource() {
        ReloadableResourceBundleMessageSource messageBundle = new ReloadableResourceBundleMessageSource();
        messageBundle.setBasename("classpath:messages/messages");
        messageBundle.setDefaultEncoding("UTF-8");
        return messageBundle;
    }


    public static void main(String[] args) {
        LOGGER.info("Sistema iniciado: "+new Date());
        SpringApplication.run(Application.class, args);
    }
}
