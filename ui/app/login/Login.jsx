import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import auth from 'consys/auth';
import Loading from 'consys/Loading';
import documentTitle from 'consys/documentTitle';
import { Row, Col, Icon, Input, Form, Button, notification } from 'antd';
import utilsCss from 'consys/utils.css';
import loginCss from './login.css';
import logo from '../../img/logo.png';
import PropTypes from 'prop-types';
const FormItem = Form.Item;

const {Password} = Input;

class Login extends Component {
  constructor() {
    super();
    this.state = {
      user: auth.user,
      entrando: false
    };
    this.logar = this.logar.bind(this);
  }
  componentDidMount() {
    documentTitle.set("Entrar");
    var that = this;
    this.onUserChange = auth.onUserChange((user) => {
      that.setState({user});
    });
  }
  componentWillUnmount () {
    auth.removeUserChange(this.onUserChange);
  }
  logar(e) {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        this.setState({entrando: true});
        let {login, senha} = values;
        auth.login({login, senha}, ({message}) => {
          notification.error({
            message,
            description: 'Usuário e/ou senha incorretos',
          });
          this.setState({entrando: false});
          this.inputSenha && this.inputSenha.focus();
        });
      }
    });
    return false;
  }
  render() {
    if (this.state.user) {
      const { from } = { from: { pathname: '/painel' } };
      return (
        <Redirect to={from}/>
      );
    }
    const { getFieldDecorator } = this.props.form;
    return (
      <Form onSubmit={this.logar} 
        hideRequiredMark={true}
        className={[utilsCss.pt3, loginCss.background].join(' ')}>
        <div>
          <Row type="flex" 
            justify="space-around" 
            align="middle">
            <Col sm={6} 
              xs={20}>
              <Row className={[utilsCss.mt2, utilsCss.s1, utilsCss.rounded, utilsCss.p2].join(' ')}
                style={{backgroundColor: '#fff'}}>
                <Loading tip="Entrando..."
                  spinning={this.state.entrando}>
                  <Row className={[utilsCss.center, utilsCss.pt2].join(' ')}>
                    <img src={logo} 
                      width="50%"
                      alt="Serviço"/>
                  </Row>
                  <Row className={utilsCss.pt4}>
                    <FormItem>
                      {getFieldDecorator('login', {
                        rules: [{ required: true, message: 'Por favor informe o usuário!' }],
                      })(
                        <Input prefix={<Icon type="user" 
                                        style={{ fontSize: 13 }} />} 
                          placeholder="Nome de usuário" />
                      )}
                    </FormItem>
                    <FormItem>
                      {getFieldDecorator('senha', {
                        rules: [{ required: true, message: 'Por favor informe a senha!' }],
                      })(
                        <Password prefix={<Icon type="lock" 
                                        style={{ fontSize: 13 }} />} 
                          type="password" 
                          placeholder="Senha"
                          ref={input => this.inputSenha = input} />
                      )}
                    </FormItem>
                  </Row>
                  <Row>
                    <Button className={utilsCss.right} 
                      type="primary" 
                      htmlType="submit">
                      Entrar
                    </Button>
                  </Row>
                </Loading>
              </Row>
            </Col>
          </Row>
        </div>
      </Form> 
    );
  }
}

Login.propTypes = {
  location: PropTypes.shape({
    state: PropTypes.shape({
      from: PropTypes.object,
    })
  }),
  form: PropTypes.object,
};

let LoginForm = Form.create()(Login);
export default LoginForm;