import React, { Component } from 'react';
import utilsCss from '../../css/utils.css';
import {Row, Input, Form, Col, Pagination, notification} from 'antd';
import documentTitle from 'consys/documentTitle';
import ReactTimeout from 'react-timeout';
import PropTypes from 'prop-types';
import Loading from 'consys/Loading';
import http from 'consys/http';
import FormattedCNPJ from 'consys/FormattedCNPJ';
import FormattedCPF from 'consys/FormattedCPF';
import {getFormatPhone} from 'consys/FormattedPhone';
import Truncate from 'consys/Truncate';
import DeleteButton from 'consys/DeleteButton';
import dataUtils from 'consys/dataUtils';
import ClienteCadastro from './ClienteCadastro';
import EditButton from 'consys/EditButton';
import NewButton from 'consys/NewButton';

const FormItem = Form.Item;

const urlClientes = '/painel/clientes';
class ClientesList extends Component {
  constructor() {
    super();
    this.state = {
      data: [],
      loading: false,
      pagination: {pagina: 1, size: 10}
    };

    this.alteraFiltro = this.alteraFiltro.bind(this);
    this.fetch = this.fetch.bind(this);
  }

  componentDidMount() {
    documentTitle.set("Clientes");
    this.fetch();
  }

  alteraFiltro(input) {
    const filtro = input.target.value;
    if (this.searchBounce) {
      this.props.clearTimeout(this.searchBounce);
    }
    this.searchBounce = this.props.setTimeout(() => {
      this.fetch({descricao: filtro});
    }, 300);
  }

  fetch(params = {pagina: 1}) {
    this.setState({ loading: true });

    if (!params.pagina) {
      params = {...params, pagina: 1};
    }

    http('/cliente/consulta', {
      method: 'POST',
      body: params
    }).then((data) => {
      const {content, size, totalElements} = data;
      this.setState({
        loading: false,
        data: content,
        pagination: {
          pagina: params.pagina,
          size, 
          totalElements
        }
      });
    }).catch((data) => {
      notification.error({
        message: 'Erro!',
        description: data.message,
      });
    });
  }

  render() {
    const { data } = this.state;
    const { size, totalElements, pagina } = this.state.pagination;
    const {del, close} = this.props.dataUtils;  
    let clientes = null;

    if (!data || !data.length){
      clientes = (
        <Row style={{ width: '100%' }}
          className={utilsCss.p4}>
          <div className={[utilsCss.center, utilsCss.h4].join(' ')}>
            Nenhum resultado encontrado
          </div>
        </Row>
      );
    } else {
      clientes = data.map(function(item, index){
        return(
          <Row key={index}
            type="flex"
            align="middle"
            className={[utilsCss.px2, utilsCss.py3, index > 0 ? utilsCss.borderTop : null].join(' ')}>
            <Col md={20}
              sm={24}
              xs={24}>
              <Row>
                <Truncate>
                  <span className={[utilsCss.bold, utilsCss.muted, utilsCss.mr2].join(' ')}>
                    {item.codigo}
                  </span> 
                  <span className={utilsCss.h3}>
                    {item.nome.toUpperCase()}
                  </span>
                </Truncate>
              </Row>
              <Row>
                <label className={utilsCss.mr1}>{item.pessoa == 'F' ? 'CPF' : 'CNPJ'}</label> 
                <b>
                  {item.pessoa == 'F' ? <FormattedCPF value={item.documento}/> : <FormattedCNPJ value={item.documento}/>}
                </b>
                &nbsp; &nbsp; &nbsp; 
                <label className={utilsCss.mr1}>Telefone</label> 
                <b>{getFormatPhone(item.telefone)}</b>
                &nbsp; &nbsp; &nbsp; 
                <label className={utilsCss.mr1}>Email</label>
                <b>{item.email}</b>
              </Row>
            </Col>
            <Col md={4}
              sm={24}
              xs={24}
              className={utilsCss.rightAlign}>
              <DeleteButton flat
                placement="topRight"
                onDelete={() =>  del(index, {
                  url: '/cliente',
                  method:'DELETE',
                  params: {codigo: item.codigo},
                  message: 'Cliente excluído com sucesso'
                })}/>
              <ClienteCadastro codigo={item.codigo}
                onClose={(res) => close(res, index)}>
                <span className={utilsCss.ml3}>
                  <EditButton flat/>
                </span>
              </ClienteCadastro>
            </Col>
          </Row>
        );
      });
    }

    return (
      <Row className={utilsCss.p2}>
        <Row className={[
            utilsCss.mt1, 
            utilsCss.mb1,
            utilsCss.pt1, 
            utilsCss.px3, 
            utilsCss.rounded, 
            utilsCss.border
          ].join(' ')}
          style={{background: '#fff'}}>
          <Col md={20}
            sm={24}
            xs={24}>
            <FormItem label="Pesquisar">
              <Input onChange={this.alteraFiltro}/>
            </FormItem>
          </Col>
          <Col md={4}
            sm={24}
            xs={24}
            className={utilsCss.rightAlign}>
            <FormItem colon={false}
              label=" ">
              <ClienteCadastro onClose={() => this.fetch()}>
                <NewButton>Novo Cliente</NewButton>
              </ClienteCadastro>
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Loading spinning={this.state.loading}
            tip="Carregando...">
            <Row style={{background: '#fff'}}
              className={[
                utilsCss.border, 
                utilsCss.px2, 
                utilsCss.rounded,
                utilsCss.mt2
              ].join(' ')}>
              {clientes}
            </Row>
          </Loading>
          <Row className={[utilsCss.mt2, utilsCss.rightAlign].join(' ')}>
            <Pagination total={totalElements}
              onChange={this.fetch}
              showTotal={(total, range) => `${range[0]}-${range[1]} de ${total} items`}
              pageSize={size}
              current={pagina}/>
          </Row>
        </Row>
      </Row>
    );
  }
}

ClientesList.propTypes = {
  setTimeout: PropTypes.func.isRequired,
  clearTimeout: PropTypes.func.isRequired,
  match: PropTypes.object,
  dataUtils: PropTypes.object
}

const ClienteListDataUtils = dataUtils(ClientesList);
const ClientesListTimeout = ReactTimeout(ClienteListDataUtils);
export default ClientesListTimeout;
export {urlClientes};