import React, { Component } from 'react';
import {Modal, Form, Button, Input, Row, Col, Radio} from 'antd';
import PropTypes from 'prop-types';
import http from 'consys/http';
import utilsCss from '../../css/utils.css';
import Loading from 'consys/Loading';
import ReactTimeout from 'react-timeout';
import {validateCpf} from 'consys/CpfInput';
import {validateCnpj} from 'consys/CnpjInput';
import CpfCnpjInput from 'consys/CpfCnpjInput';
import PhoneInput from 'consys/PhoneInput';
import CepTypeahead from 'consys/CepTypeahead';

const RadioGroup = Radio.Group;
const FormItem = Form.Item;

class ClienteCadastro extends Component {
  constructor() {
    super();
    this.state = {
      visible: false, 
      loading: true,
      tipo: 'F'
    };

    this.handleCancel = this.handleCancel.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.modal = this.modal.bind(this);
    this.handleChangeType = this.handleChangeType.bind(this);
    this.handleChangeCep = this.handleChangeCep.bind(this);
  }
  
  modal(visible) {
    const that = this;
    const {codigo, form} = this.props;
    this.setState({visible}, () => {
      setTimeout(()=>{this.inputFocus && this.inputFocus.focus()}, 1);
    });
    
    if (visible) {
      if (codigo) {
        http('/cliente', {
          method: 'GET',
          params: {codigo: codigo},
        }).then((data) => {
          const {pessoa} = data;
          that.setState({
            loading: false,
            tipoOriginal: pessoa ? pessoa : 'F',
          });
          delete data.codigo;
          form.setFieldsValue(data);
        });
      }
      else {
        that.setState({loading: false});
        form.resetFields();
        form.setFieldsValue({pessoa: 'F'});
      }
    }
  }
  
  handleSubmit(e) {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        this.setState({confirmLoading: true});
        const {onClose, codigo} = this.props;

        const obj = {
          codigo: codigo ? codigo : 0,
          ...values
        };

        http('/cliente', {
          method: 'POST',
          body: obj,
        }).then((res) => {
          Modal.success({
            title: 'Sucesso',
            content: codigo ? 'Atualizado com Sucesso' : 'Criado com Sucesso',
          });
          this.setState({
            visible: false,
            confirmLoading: false
          });
          onClose && onClose(res);
        }).catch((err) => {
          this.setState({confirmLoading: false});
          Modal.error({
            title: 'Erro',
            content: err.message,
          });
        });
      }
    });
  }

  handleCancel() {
    this.setState({visible: false});
    this.props.form.resetFields();
  }

  handleChangeType(e){
    const tipo = e.target.value;
    const {form} = this.props;
    let values = {};

    if (tipo == this.state.tipoOriginal) {
      values = {documento: this.state.documento};
    }
    else {
      form.resetFields(['documento']);
    }
       
    setTimeout(()=>{
      form.setFieldsValue(values);
    }, 250);
    this.setState({tipo});
  }

  handleChangeCep(values){
    const {bairro, complemento, localidade, logradouro, uf, cep} = values;
    this.props.form.setFieldsValue({
      cep: cep,
      bairro, 
      rua: logradouro, 
      cidade: localidade, 
      complemento, 
      estado: uf
    });
  }

  render() {
    const {getFieldDecorator} = this.props.form;
    const {confirmLoading, tipo} = this.state;
    
    return (
      <span>
        <span onClick={() => { this.modal(true) }}
          className={utilsCss.pointer}>
          {this.props.children}
        </span>
        <Modal width="70%"
          centered
          visible={this.state.visible}
          destoyOnClose
          onOk={this.handleSubmit}
          onCancel={this.handleCancel}
          footer={[
            <Button key="back" 
              onClick={this.handleCancel}>
              Cancelar
            </Button>,
            <Button key="submit" 
              type="primary"
              onClick={this.handleSubmit}
              loading={confirmLoading}>
              Salvar
            </Button>
          ]}>
          <Loading tip="Carregando..."
            spinning={this.state.loading}>
            <Row className={[utilsCss.h2, utilsCss.bold].join(' ')}>
              Cliente
            </Row>
            <Row className={utilsCss.h4}>
              {this.props.codigo ? 'Editando cliente: '+this.props.codigo : 'Novo cliente'}
            </Row>
            <Row className={[utilsCss.h4, utilsCss.mb2].join(' ')}>
              Campos marcados com 
              <span style={{color: '#f5222d', fontFamily: 'SimSun', fontSize: '13px'}}>
                &nbsp;*&nbsp;
              </span>
              são obrigatórios
            </Row>
            <Form onSubmit={this.handleSubmit}>
              <Row gutter={16}>
                <Col md={12}
                  sm={24}
                  xs={24}>
                  <FormItem label="Nome">
                    {getFieldDecorator('nome', {
                      rules: [{
                        required: true,
                        message: 'Nome é obrigatório, não pode ficar em branco'
                      }]
                    })(
                      <Input ref={(input) => this.inputFocus = input}/>
                    )}
                  </FormItem>
                </Col>
                <Col md={5}
                  sm={24}
                  xs={24}>
                  <FormItem label="Pessoa">
                    {getFieldDecorator('pessoa',{
                      rules: [{required: true}]
                    })(
                      <RadioGroup onChange={this.handleChangeType}>
                        <Radio value="F">Física</Radio>
                        <Radio value="J">Jurídica</Radio>
                      </RadioGroup>
                    )}
                  </FormItem>
                </Col>
                <Col md={7}
                  sm={24}
                  xs={24}>
                  <FormItem label={tipo == 'F' ? 'CPF' : 'CNPJ'}
                    hasFeedback>
                    {getFieldDecorator('documento', {
                      rules: [{ 
                        required: true, message: 'Documento é obrigatório, não pode ficar em branco' 
                      }, {
                        validator: tipo == 'F' ? validateCpf : validateCnpj
                      }]
                    })(
                      <CpfCnpjInput type={tipo == 'F' ? 'CPF' : 'CNPJ'}/>
                    )}
                  </FormItem>
                </Col>
              </Row>
              <Row gutter={16}>
                <Col md={12}
                  sm={24}
                  xs={24}>
                  <FormItem label="E-mail"
                    hasFeedback>
                    {getFieldDecorator('email', {
                      rules: [
                        {type: 'email', message: 'O e-mail informado não é válido'},
                      ],
                    })(
                      <Input />
                    )}
                  </FormItem>
                </Col>
                <Col md={12}
                  sm={24}
                  xs={24}>
                  <FormItem label="Telefone">
                    {getFieldDecorator('telefone')(
                      <PhoneInput />
                    )}
                  </FormItem>
                </Col>
              </Row>
              <Row gutter={16}>
                <Col sm={12}
                  xs={24}>
                  <FormItem label="Cep">
                    {getFieldDecorator('cep', {
                      rules: [{
                        required: true,
                        message: 'Cep é obrigatório, não pode ficar em branco'
                      }]
                    })(
                      <CepTypeahead onSuccess={this.handleChangeCep}
                        onFail={() => this.props.form.resetFields(['bairro', 'rua', 'cidade', 'complemento', 'numero', 'estado'])}/>
                    )}
                  </FormItem>
                </Col>
                <Col sm={9}
                  xs={12}>
                  <FormItem label="Cidade">
                    {getFieldDecorator('cidade', {
                      rules: [{
                        required: true,
                        message: 'Cidade é obrigatória, não pode ficar em branco'
                      }]
                    })(
                      <Input disabled/>
                    )}
                  </FormItem>
                </Col>
                <Col sm={3}
                  xs={12}>
                  <FormItem label="Estado">
                    {getFieldDecorator('estado', {
                      rules: [{
                        required: true,
                        message: 'Estado é obrigatório, não pode ficar em branco'
                      }]
                    })(
                      <Input disabled/>
                    )}
                  </FormItem>
                </Col>
                <Col sm={10}
                  xs={24}>
                  <FormItem label="Bairro">
                    {getFieldDecorator('bairro', {
                      rules: [{
                        required: true,
                        message: 'Bairro é obrigatório, não pode ficar em branco'
                      }]
                    })(
                      <Input />
                    )}
                  </FormItem>
                </Col>
                <Col sm={10}
                  xs={24}>
                  <FormItem label="Rua">
                    {getFieldDecorator('rua', {
                      rules: [{
                        required: true,
                        message: 'Rua é obrigatória, não pode ficar em branco'
                      }]
                    })(
                      <Input />
                    )}
                  </FormItem>
                </Col>
                <Col sm={4}
                  xs={24}>
                  <FormItem label="Número">
                    {getFieldDecorator('numero', {
                      rules: [{
                        required: true,
                        message: 'Número é obrigatório, não pode ficar em branco'
                      }]
                    })(
                      <Input/>
                    )}
                  </FormItem>
                </Col>
                <Col span={24}>
                  <FormItem label="Complemento">
                    {getFieldDecorator('complemento')(
                      <Input/>
                    )}
                  </FormItem>
                </Col>
              </Row>
            </Form>
          </Loading>
        </Modal>
      </span>
    );
   }
}

ClienteCadastro.propTypes = {
  onClose: PropTypes.func,
  form: PropTypes.object,
  codigo: PropTypes.number,
  children: PropTypes.node
};

const ClienteCadastroForm = Form.create()(ClienteCadastro);
const ClienteCadastroTimeout = ReactTimeout(ClienteCadastroForm);
export default ClienteCadastroTimeout;