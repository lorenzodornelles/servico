import React, { Component } from 'react';
import Typeahead from 'consys/Typeahead';
import Lookup from 'consys/Lookup';
import http from 'consys/http';
import CpfCnpjInput from 'consys/CpfCnpjInput';
import PropTypes from 'prop-types';
import {Input, Form, Row, Col, InputNumber} from 'antd';
import createFilter from 'consys/createFilter';
import ReactTimeout from 'react-timeout';
import FormattedCNPJ from 'consys/FormattedCNPJ';
import FormattedCPF from 'consys/FormattedCPF';

const FormItem = Form.Item;

function loadCliente(cliente, callback) {
  if (!cliente) {
    callback && callback(cliente);
    return;
  }
  http('/cliente', {
    params: {codigo: cliente.codigo}
  }).then((cli) => {
    cliente = cli;
    callback && callback(cliente);
  })
}

class ClienteTypeahead extends Component {
  constructor() {
    super();
    this.state = {};
    this.columns = [
      {
        title: 'Cod',
        dataIndex: 'codigo',
        sorter: true,
      },
      {
        title: 'Nome',
        dataIndex: 'nome'
      },
      {
        title: 'Documento',
        render: (item) =>
          item && item.pessoa && item.pessoa == 'J' ? 
          <FormattedCNPJ value={item.documento}/> : <FormattedCPF value={item.documento}/>
      }
    ];
    this.handleChange = this.handleChange.bind(this);
    this.handleBusca = this.handleBusca.bind(this);
    this.handleSearchReady = this.handleSearchReady.bind(this);
  }
  handleChange(cliente) {
    const {onChange} = this.props;
    if (cliente)
    {
      loadCliente(cliente, onChange);
    }
  }
  handleBusca(params) {
    let {codigo, nome, documento} = this.state;
    
    if (params) {
      if (params.codigo) {
        codigo = params.codigo;
      }
      if (params.nome) {
        nome = params.nome;
      }
      if (params.documento) {
        documento = params.documento;
      }

      this.setState({
        ...params
      });
    }

    if (this.searchBounce)
    {
      this.props.clearTimeout(this.searchBounce);
    }
    this.setState({
      loading: true
    });
    this.searchBounce = this.props.setTimeout(() => {
      const body = {
        filters: {
          ...createFilter('codigo', codigo),
          ...createFilter('nome', nome),
          ...createFilter('documento', documento)
        }
      }
      this.search(body);
    }, 900);
  }
  handleSearchReady(search) { 
    this.search = search;
  }
  render() {
    const {data} = this.state;
    const lookup = (
      <Lookup 
        body={
          <Row gutter={16}>
            <Col span={4}>
              <FormItem label="Código">
                <InputNumber style={{width: '100%'}}
                  onChange={(codigo) => this.handleBusca({codigo})}/>
              </FormItem>
            </Col>
            <Col span={12}>
              <FormItem label="Nome">
                <Input onChange={(e) => this.handleBusca({nome: e.target.value})}/>
              </FormItem>
            </Col>
            <Col span={8}>
              <FormItem label="CPF/CNPJ">
                <CpfCnpjInput onChange={(documento) => this.handleBusca({documento})}/>
              </FormItem>
            </Col>
          </Row>
        }
        onSearchReady={this.handleSearchReady}
        data={data}
        title="Clientes"
        columns={this.columns} 
        url="/cliente/lookup"
        rowKey={item => item.codigo}
        width={800}
      />
    )
    return (
      <Typeahead {...this.props}
        onChange={this.handleChange}
        title="Clientes"
        columns={this.columns} 
        url="/cliente/typeahead"
        lookup={lookup}
        view={item => item ? item.codigo+' - '+item.nome : null}/>
    );
  }
}

ClienteTypeahead.propTypes = {
  onChange: PropTypes.func,
  setTimeout: PropTypes.func,
  clearTimeout: PropTypes.func
}

const ClienteTypeaheadTimeout = ReactTimeout(ClienteTypeahead);
export default ClienteTypeaheadTimeout;
export {loadCliente};