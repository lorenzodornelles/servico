import React, { Component } from 'react';
import {IntlProvider, addLocaleData } from 'react-intl';
import AppRouter from '../routes/AppRouter';
import { LocaleProvider } from 'antd';
import ptBR from 'antd/lib/locale-provider/pt_BR';
import pt from 'react-intl/locale-data/pt';
import favicon from 'consys/favicon';
import addressBar from 'consys/addressBar';
import Config from 'consys/Config';
import faviconImage from '../../img/favicon.png';
import '../auth/authConfig';

addLocaleData(pt);
favicon.set({
  url: faviconImage
});
addressBar.setColor({color: '#2B398F'})

Config.set({
  loginUrl: '/entrar', 
  projectName: 'Serviços Online'
});

class Root extends Component {
  render() {
      return (
        <LocaleProvider locale={ptBR}>
          <IntlProvider locale="pt">
            <AppRouter/>
          </IntlProvider>
        </LocaleProvider>
      );
   }
}
export default Root;