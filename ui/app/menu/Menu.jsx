import React, { Component } from 'react';
import { Row, Col, Tag } from 'antd';
import auth from 'consys/auth';
import utilsCss from '../../css/utils.css';
import menuCss from './menu.css';
import MenuItem from './MenuItem';
import {urlResumo} from '../resumo/Resumo';
import {urlServicos} from '../servicos/ServicosList';
import {urlClientes} from '../clientes/ClientesList';
import {urlUsuarios} from '../usuarios/UsuariosList';

class _Menu extends Component {
  constructor() {
    super();
    this.state = {
      current: 'geral',
      user: auth.user,
      mostraConfirmaSair: false,
    };
    this.confirmaSair = this.confirmaSair.bind(this);
  }
  confirmaSair() {
    auth.logout();
    this.setState({user: false});
  }
  render() {
    const {user} = this.state;
    return (
      <Row className={menuCss.menuBackground}>
        <Row className={[utilsCss.px2, utilsCss.py1, utilsCss.muted].join(' ')}>
          Usuário <b>{user.descricao}</b>&nbsp;
          {user.admin ? 
            <span className={[utilsCss.h6, utilsCss.pl2].join(' ')}>
              <Tag color="gray">Administrador</Tag>
            </span>
          : null}
        </Row>
        <Row type="flex" 
          justify="space-around" 
          align="middle">
          <Col md={18}
            xs={24}>
            <Col span={user.admin ? 5 : 6}>
              <MenuItem url={urlResumo} 
                icon="home">Início</MenuItem>
            </Col>
            <Col span={user.admin ? 5 : 6}>
              <MenuItem url={urlClientes} 
                icon="user">Clientes</MenuItem>
            </Col>
            <Col span={user.admin ? 5 : 6}>
              <MenuItem url={urlServicos} 
                icon="reconciliation">Serviços</MenuItem>
            </Col>
            {user.admin ?
              <Col span={5}>
                <MenuItem url={urlUsuarios} 
                  icon="team">Usuários</MenuItem>
              </Col>
             : null}
            <Col span={user.admin ? 4 : 6}>
              <MenuItem onClick={this.confirmaSair}
                icon="poweroff">Sair</MenuItem>
            </Col>
          </Col>
        </Row>
      </Row>
    );
  }
}

export default _Menu;