import React, { Component } from 'react';
import { Router, Route, Switch } from 'react-router-dom';
import { PrivateRoute } from 'consys/Route';
import createHistory from 'history/createBrowserHistory';
import asyncComponent from 'consys/asyncComponent';
const history = createHistory();

class AppRouter extends Component {
  constructor() {
    super();
  }
  render() {
    return (
      <Router history={history}>
        <Switch>
          <Route exact 
            path="/" 
            component={asyncComponent(() => System.import('../login/Login'))} />
          <Route exact 
            path="/entrar" 
            component={asyncComponent(() => System.import('../login/Login'))} />
          <PrivateRoute 
            path="/painel" 
            component={asyncComponent(() => System.import('../painel/Painel'))} />
          <Route component={asyncComponent(() => System.import('../naoEncontrado/NaoEncontrado'))}/>
        </Switch>
      </Router> 
    );
  }
}

export default AppRouter;
export {history};