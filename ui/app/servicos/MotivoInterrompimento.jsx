import React, { Component } from 'react';
import {Modal, Form, Button, Input, Row} from 'antd';
import PropTypes from 'prop-types';
import http from 'consys/http';
import utilsCss from '../../css/utils.css';
import ReactTimeout from 'react-timeout';

const FormItem = Form.Item;
const { TextArea } = Input;

class MotivoInterrompimento extends Component {
  constructor() {
    super();
    this.state = {
      visible: false
    };

    this.handleCancel = this.handleCancel.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.modal = this.modal.bind(this);
  }
  
  modal(visible, e) {
    e.stopPropagation();
    this.props.form.resetFields();
    this.setState({visible}, () => {
      setTimeout(()=>{this.inputFocus && this.inputFocus.focus()}, 1);
    });
  }
  
  handleSubmit(e) {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        this.setState({confirmLoading: true});
        const {onClose, servico} = this.props;

        const obj = {
          ...servico,
          ...values,
          status: 'I',
          fim: new Date()
        };

        http('/servico', {
          method: 'POST',
          body: obj,
        }).then((res) => {
          Modal.success({
            title: 'Sucesso',
            content: 'Serviço foi interrompido',
          });
          this.setState({
            visible: false,
            confirmLoading: false
          });
          onClose && onClose(res);
        }).catch((err) => {
          this.setState({confirmLoading: false});
          Modal.error({
            title: 'Erro',
            content: err.message,
          });
        });
      }
    });
  }

  handleCancel() {
    this.setState({visible: false});
    this.props.form.resetFields();
  }

  render() {
    const {getFieldDecorator} = this.props.form;
    const {confirmLoading} = this.state;
    
    return (
      <span>
        <span onClick={(e) => { this.modal(true, e) }}
          className={utilsCss.pointer}>
          {this.props.children}
        </span>
        <Modal width="40%"
          centered
          visible={this.state.visible}
          destoyOnClose
          onOk={this.handleSubmit}
          onCancel={this.handleCancel}
          onClick={(e) => e.stopPropagation()}
          footer={[
            <Button key="back" 
              onClick={this.handleCancel}>
              Cancelar
            </Button>,
            <Button key="submit" 
              type="primary"
              onClick={this.handleSubmit}
              loading={confirmLoading}>
              Salvar
            </Button>
          ]}>
          <Row className={[utilsCss.h2, utilsCss.bold].join(' ')}>
            Motivo de interrompimento
          </Row>
          <Row className={[utilsCss.h4, utilsCss.mb2].join(' ')}>
            Campos marcados com 
            <span style={{color: '#f5222d', fontFamily: 'SimSun', fontSize: '13px'}}>
              &nbsp;*&nbsp;
            </span>
            são obrigatórios
          </Row>
          <Form onSubmit={this.handleSubmit}>
            <FormItem label="Motivo">
              {getFieldDecorator('motivo', {
                rules: [{
                  required: true,
                  message: 'Motivo é obrigatório, não pode ficar em branco'
                }]
              })(
                <TextArea rows={4} 
                  ref={(input) => this.inputFocus = input}/>
              )}
            </FormItem>
          </Form>
        </Modal>
      </span>
    );
   }
}

MotivoInterrompimento.propTypes = {
  onClose: PropTypes.func,
  form: PropTypes.object,
  servico: PropTypes.object,
  children: PropTypes.node
};

const MotivoInterrompimentoForm = Form.create()(MotivoInterrompimento);
const MotivoInterrompimentoTimeout = ReactTimeout(MotivoInterrompimentoForm);
export default MotivoInterrompimentoTimeout;