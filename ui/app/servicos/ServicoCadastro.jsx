import React, { Component } from 'react';
import {Modal, Form, Button, Input, Row, Col} from 'antd';
import PropTypes from 'prop-types';
import http from 'consys/http';
import utilsCss from '../../css/utils.css';
import Loading from 'consys/Loading';
import ReactTimeout from 'react-timeout';
import ClienteTypeahead from '../clientes/ClienteTypeahead';
import UsuarioTypeahead from '../usuarios/UsuarioTypeahead';

const FormItem = Form.Item;

class ServicoCadastro extends Component {
  constructor() {
    super();
    this.state = {
      visible: false, 
      loading: true,
      tipo: 'F'
    };

    this.handleCancel = this.handleCancel.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.modal = this.modal.bind(this);
  }
  
  modal(visible) {
    const that = this;
    const {codigo, form} = this.props;
    this.setState({visible}, () => {
      setTimeout(()=>{this.inputFocus && this.inputFocus.focus()}, 1);
    });
    
    if (visible) {
      if (codigo) {
        http('/servico', {
          method: 'GET',
          params: {codigo: codigo},
        }).then((data) => {
          const {encarregado, cliente, tipo, marca, problema} = data;
          that.setState({
            loading: false,
            data
          });
          form.setFieldsValue({encarregado, cliente, tipo, marca, problema});
        });
      }
      else {
        that.setState({loading: false});
        form.resetFields();
      }
    }
  }
  
  handleSubmit(e) {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        this.setState({confirmLoading: true});
        const {onClose, codigo} = this.props;
        const {data} = this.state;

        const obj = {
          codigo: codigo ? codigo : 0,
          ...data,
          ...values,
          status: data && data.status ? data.status : 'P'
        };

        http('/servico', {
          method: 'POST',
          body: obj,
        }).then((res) => {
          Modal.success({
            title: 'Sucesso',
            content: codigo ? 'Atualizado com Sucesso' : 'Criado com Sucesso',
          });
          this.setState({
            visible: false,
            confirmLoading: false
          });
          onClose && onClose(res);
        }).catch((err) => {
          this.setState({confirmLoading: false});
          Modal.error({
            title: 'Erro',
            content: err.message,
          });
        });
      }
    });
  }

  handleCancel() {
    this.setState({visible: false});
    this.props.form.resetFields();
  }

  render() {
    const {getFieldDecorator} = this.props.form;
    const {confirmLoading} = this.state;
    
    return (
      <span>
        <span onClick={() => { this.modal(true) }}
          className={utilsCss.pointer}>
          {this.props.children}
        </span>
        <Modal width="70%"
          centered
          visible={this.state.visible}
          destoyOnClose
          onOk={this.handleSubmit}
          onCancel={this.handleCancel}
          footer={[
            <Button key="back" 
              onClick={this.handleCancel}>
              Cancelar
            </Button>,
            <Button key="submit" 
              type="primary"
              onClick={this.handleSubmit}
              loading={confirmLoading}>
              Salvar
            </Button>
          ]}>
          <Loading tip="Carregando..."
            spinning={this.state.loading}>
            <Row className={[utilsCss.h2, utilsCss.bold].join(' ')}>
              Serviço
            </Row>
            <Row className={utilsCss.h4}>
              {this.props.codigo ? 'Editando serviço: '+this.props.codigo : 'Novo serviço'}
            </Row>
            <Row className={[utilsCss.h4, utilsCss.mb2].join(' ')}>
              Campos marcados com 
              <span style={{color: '#f5222d', fontFamily: 'SimSun', fontSize: '13px'}}>
                &nbsp;*&nbsp;
              </span>
              são obrigatórios
            </Row>
            <Form onSubmit={this.handleSubmit}>
              <Row gutter={16}>
                <Col md={12}
                  sm={24}
                  xs={24}>
                  <FormItem label="Cliente">
                    {getFieldDecorator('cliente',{
                      rules: [{ required: true, message: 'O cliente é obrigatório e não pode ficar em branco', type: 'object' }]
                    })(
                      <ClienteTypeahead />
                    )}
                  </FormItem>
                </Col>
                <Col md={12}
                  sm={24}
                  xs={24}>
                  <FormItem label="Encarregado">
                    {getFieldDecorator('encarregado',{
                      rules: [{ required: true, message: 'Informe um encarregado para o serviço', type: 'object' }]
                    })(
                      <UsuarioTypeahead />
                    )}
                  </FormItem>
                </Col>
              </Row>
              <Row gutter={16}>
                <Col md={4}
                  sm={24}
                  xs={24}>
                  <FormItem label="Tipo">
                    {getFieldDecorator('tipo', {
                      rules: [
                        {required: true, message: 'O tipo do equipamento é obrigatório'},
                      ],
                    })(
                      <Input />
                    )}
                  </FormItem>
                </Col>
                <Col md={4}
                  sm={24}
                  xs={24}>
                  <FormItem label="Marca">
                    {getFieldDecorator('marca', {
                      rules: [
                        {required: true, message: 'A marca do equipamento é obrigatório'},
                      ],
                    })(
                      <Input />
                    )}
                  </FormItem>
                </Col>
                <Col md={16}
                  sm={24}
                  xs={24}>
                  <FormItem label="Problema">
                    {getFieldDecorator('problema', {
                      rules: [
                        {required: true, message: 'Informe um problema'},
                      ],
                    })(
                      <Input />
                    )}
                  </FormItem>
                </Col>
              </Row>
            </Form>
          </Loading>
        </Modal>
      </span>
    );
   }
}

ServicoCadastro.propTypes = {
  onClose: PropTypes.func,
  form: PropTypes.object,
  codigo: PropTypes.number,
  children: PropTypes.node
};

const ServicoCadastroForm = Form.create()(ServicoCadastro);
const ServicoCadastroTimeout = ReactTimeout(ServicoCadastroForm);
export default ServicoCadastroTimeout;