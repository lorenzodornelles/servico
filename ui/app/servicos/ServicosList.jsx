import React, { Component } from 'react';
import utilsCss from '../../css/utils.css';
import {Row, Input, Form, Col, Pagination, notification, Radio} from 'antd';
import documentTitle from 'consys/documentTitle';
import ReactTimeout from 'react-timeout';
import PropTypes from 'prop-types';
import Loading from 'consys/Loading';
import http from 'consys/http';
import DeleteButton from 'consys/DeleteButton';
import dataUtils from 'consys/dataUtils';
import ServicoCadastro from './ServicoCadastro';
import EditButton from 'consys/EditButton';
import NewButton from 'consys/NewButton';
import {FormattedDate} from 'react-intl';
import TagCustom from '../tagCustom/TagCustom';

const RadioGroup = Radio.Group;
const FormItem = Form.Item;

const urlServicos = '/painel/servicos';
class ServicosList extends Component {
  constructor() {
    super();
    this.state = {
      data: [],
      loading: false,
      pagination: {pagina: 1, size: 10},
      status: 'T'
    };

    this.alteraFiltro = this.alteraFiltro.bind(this);
    this.fetch = this.fetch.bind(this);
    this.handleChangeStatus = this.handleChangeStatus.bind(this);
  }

  handleChangeStatus(e){
    const status = e.target.value;
       
    setTimeout(()=>{
      this.fetch({status});
    }, 250);
    this.setState({status});
  }

  componentDidMount() {
    documentTitle.set("Serviços");
    this.fetch();
  }

  alteraFiltro(input) {
    const filtro = input.target.value;
    if (this.searchBounce) {
      this.props.clearTimeout(this.searchBounce);
    }
    this.searchBounce = this.props.setTimeout(() => {
      this.fetch({descricao: filtro});
    }, 300);
  }

  fetch(params = {pagina: 1}) {
    this.setState({ loading: true });

    if (!params.pagina) {
      params = {...params, pagina: 1};
    }
    if (!params.status)  {
      params = {...params, status: 'T'};
    }

    http('/servico/consulta', {
      method: 'POST',
      body: params
    }).then((data) => {
      const {content, size, totalElements} = data;
      this.setState({
        loading: false,
        data: content,
        pagination: {
          pagina: params.pagina,
          size, 
          totalElements
        }
      });
    }).catch((data) => {
      notification.error({
        message: 'Erro!',
        description: data.message,
      });
    });
  }

  render() {
    const { data, status } = this.state;
    const { size, totalElements, pagina } = this.state.pagination;
    const {del, close} = this.props.dataUtils;  
    let servicos = null;

    if (!data || !data.length){
      servicos = (
        <Row style={{ width: '100%' }}
          className={utilsCss.p4}>
          <div className={[utilsCss.center, utilsCss.h4].join(' ')}>
            Nenhum resultado encontrado
          </div>
        </Row>
      );
    } else {
      servicos = data.map(function(item, index){
        return(
          <Row key={index}
            type="flex"
            align="middle"
            className={[utilsCss.px2, utilsCss.py3, index > 0 ? utilsCss.borderTop : null].join(' ')}>
            <Col md={20}
              sm={24}
              xs={24}>
              <Row>
                <span className={[utilsCss.bold, utilsCss.muted, utilsCss.mr2].join(' ')}>
                  N° {item.codigo}
                </span> 
                <span className={[utilsCss.h3, utilsCss.bold].join(' ')}>
                  {item.tipo}
                </span>
                <span className={[utilsCss.ml2, utilsCss.bold].join(' ')}>
                  <small className={utilsCss.mr1}>MARCA:</small>
                  {item.marca}
                </span>
              </Row>
              <Row>
                {item.status == 'P' ? 
                  <span>
                    <TagCustom color="#3498db"
                      className={utilsCss.mr1}>
                      Pendente
                    </TagCustom>
                    {item.inicio ?
                      <TagCustom color="#3498db">
                        <small className={utilsCss.mr1}>COMEÇOU:</small>
                        <FormattedDate value={item.inicio}/>
                      </TagCustom>
                     : null}
                  </span>
                : null}
                {item.status == 'I' ? 
                  <span>
                    <TagCustom color="#34495e"
                      className={utilsCss.mr1}>
                      Interrompido
                    </TagCustom> 
                    <TagCustom color="#34495e">
                      <span className={utilsCss.mr2}>
                        <small className={utilsCss.mr1}>COMEÇOU:</small>
                        <FormattedDate value={item.inicio}/>
                      </span>
                      <span>
                        <small className={utilsCss.mr1}>INTERROMPEU:</small>
                        <FormattedDate value={item.fim}/>
                      </span>
                    </TagCustom>
                  </span>
                : null}
                {item.status == 'F' ? 
                  <span>
                    <TagCustom color="#2ecc71"
                      className={utilsCss.mr1}>
                      Finalizado
                    </TagCustom>
                    <TagCustom color="#2ecc71">
                      <span className={utilsCss.mr2}>
                        <small className={utilsCss.mr1}>COMEÇOU:</small>
                        <FormattedDate value={item.inicio}/>
                      </span>
                      <span>
                        <small className={utilsCss.mr1}>FINALIZOU:</small>
                        <FormattedDate value={item.fim}/>
                      </span>
                    </TagCustom>
                  </span>
                : null}
              </Row>
              <Row>
                <span className={utilsCss.mr2}>
                  <small className={[utilsCss.mr1, utilsCss.bold].join(' ')}>CLIENTE:</small>
                  {item.cliente.nome}
                </span>
                <span className={utilsCss.mr2}>
                  <small className={[utilsCss.mr1, utilsCss.bold].join(' ')}>ENCARREGADO:</small>
                  {item.encarregado.nome}
                </span>
                <span className={utilsCss.mr2}>
                  <small className={[utilsCss.mr1, utilsCss.bold].join(' ')}>CADASTRADO EM:</small>
                  <FormattedDate value={item.cadastro}/>
                </span>
              </Row>
            </Col>
            <Col md={4}
              sm={24}
              xs={24}
              className={utilsCss.rightAlign}>
              <DeleteButton flat
                placement="topRight"
                onDelete={() =>  del(index, {
                  url: '/servico',
                  method:'DELETE',
                  params: {codigo: item.codigo},
                  message: 'Serviço excluído com sucesso'
                })}/>
              <ServicoCadastro codigo={item.codigo}
                onClose={(res) => close(res, index)}>
                <span className={utilsCss.ml3}>
                  <EditButton flat/>
                </span>
              </ServicoCadastro>
            </Col>
          </Row>
        );
      });
    }

    return (
      <Row className={utilsCss.p2}>
        <Row className={[
            utilsCss.mt1, 
            utilsCss.mb1,
            utilsCss.pt1, 
            utilsCss.px3, 
            utilsCss.rounded, 
            utilsCss.border
          ].join(' ')}
          style={{background: '#fff'}}>
          <Col md={20}
            sm={24}
            xs={24}>
            <FormItem label="Pesquisar"
              extra={
                <RadioGroup value={status}
                  onChange={this.handleChangeStatus}>
                  <Radio value="T">Todos</Radio>
                  <Radio value="P">Pendentes</Radio>
                  <Radio value="F">Finalizados</Radio>
                  <Radio value="I">Interrompidos</Radio>
                </RadioGroup>
              }>
              <Input onChange={this.alteraFiltro}/>
            </FormItem>
          </Col>
          <Col md={4}
            sm={24}
            xs={24}
            className={utilsCss.rightAlign}>
            <FormItem colon={false}
              label=" ">
              <ServicoCadastro onClose={() => this.fetch()}>
                <NewButton>Novo Serviço</NewButton>
              </ServicoCadastro>
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Loading spinning={this.state.loading}
            tip="Carregando...">
            <Row style={{background: '#fff'}}
              className={[
                utilsCss.border, 
                utilsCss.px2, 
                utilsCss.rounded,
                utilsCss.mt2
              ].join(' ')}>
              {servicos}
            </Row>
          </Loading>
          <Row className={[utilsCss.mt2, utilsCss.rightAlign].join(' ')}>
            <Pagination total={totalElements}
              onChange={this.fetch}
              showTotal={(total, range) => `${range[0]}-${range[1]} de ${total} items`}
              pageSize={size}
              current={pagina}/>
          </Row>
        </Row>
      </Row>
    );
  }
}

ServicosList.propTypes = {
  setTimeout: PropTypes.func.isRequired,
  clearTimeout: PropTypes.func.isRequired,
  match: PropTypes.object,
  dataUtils: PropTypes.object
}

const ClienteListDataUtils = dataUtils(ServicosList);
const ServicosListTimeout = ReactTimeout(ClienteListDataUtils);
export default ServicosListTimeout;
export {urlServicos};