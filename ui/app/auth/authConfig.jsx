import auth from 'consys/auth';
import http from 'consys/http';

auth.init(function({login, senha}, callback) {
  if (this.user) {
    return;
  }
  var that = this;
  const body = {login, senha};
  return http('/entrar', {
    method: 'POST',
    body
  }).then(function(res) {
    that.userChange(res);
    return true;
  }).catch((err) => {
    callback && callback(err);
  });
}, function(callback) {
  this.userChange(false);
  callback && callback();
});