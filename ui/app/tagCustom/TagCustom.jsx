import React, { Component } from 'react';
import {Tag} from 'antd';
import PropTypes from 'prop-types';

class TagCustom extends Component {
  render() {
    return(
      <Tag color={this.props.color}
        {...this.props}>
        <b>{this.props.children}</b>
      </Tag>
    );
  }
}

TagCustom.propTypes = {
  color: PropTypes.string,
  children: PropTypes.node
}
export default TagCustom;
