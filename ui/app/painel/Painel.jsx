import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';
import Menu from '../menu/Menu';
import { PrivateRoute } from 'consys/Route';
import Resumo from '../resumo/Resumo';
import ServicosList, {urlServicos} from '../servicos/ServicosList';
import ClientesList, {urlClientes} from '../clientes/ClientesList';
import UsuariosList, {urlUsuarios} from '../usuarios/UsuariosList';
import NotFound from 'consys/NotFound';
import PropTypes from 'prop-types';
import Footer from '../footer/Footer';

class Painel extends Component {
  render() {
    const { match } = this.props;
    const { url } = match;
    return (
      <div>
        <Menu match={match}/> 
        <Switch>
          <PrivateRoute exact 
            path={`${url}`} 
            component={Resumo} />
          <PrivateRoute exact 
            path={urlServicos} 
            component={ServicosList} />
          <PrivateRoute exact 
            path={urlClientes} 
            component={ClientesList} />
          <PrivateRoute exact 
            path={urlUsuarios} 
            component={UsuariosList} />
          <Route component={NotFound}/>
        </Switch>
        <Footer/>
      </div>
    );
  }
}

Painel.propTypes = {
  match: PropTypes.shape({
    url: PropTypes.string,
  }),
};

export default Painel;