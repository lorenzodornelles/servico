import React, { Component } from 'react';
import Screen from 'consys/Screen';
import Truncate from 'consys/Truncate';
import http from 'consys/http';
import documentTitle from 'consys/documentTitle';
import utilsCss from '../../css/utils.css';
import {Row, Col, Card, Modal, Collapse, Tooltip, Icon, Popconfirm} from 'antd';
import ReactDOM from 'react-dom';
import Loading from 'consys/Loading';
import FormattedCNPJ from 'consys/FormattedCNPJ';
import FormattedCPF from 'consys/FormattedCPF';
import {getFormatPhone} from 'consys/FormattedPhone';
import TagCustom from '../tagCustom/TagCustom';
import {FormattedDate} from 'react-intl';
import MotivoInterrompimento from '../servicos/MotivoInterrompimento';

const Panel = Collapse.Panel;

const urlResumo = "/painel";
class Resumo extends Component {
  constructor() {
    super();
    this.state = {
      clientesRecentes: [],
      servicos: [],
      loadingClientesRecentes: true,
      loadingServicosPendentes: true
    };
    
    this.buscaClientesRecentes = this.buscaClientesRecentes.bind(this);
    this.buscaServicosPendentes = this.buscaServicosPendentes.bind(this);
    this.ref = this.ref.bind(this);
    this.error = this.error.bind(this);
    this.handleSaveService = this.handleSaveService.bind(this);
  }

  error(title, {message} = {}) {
    Modal.error({
      title,
      content: message
    });
  }

  buscaClientesRecentes() {
    http('/cliente/recentes',{
      method: 'GET'
    }).then((data) => {
      this.setState({loadingClientesRecentes: false, clientesRecentes: data});
      this.buscaServicosPendentes();
    }).catch((e) => {
      this.setState({loadingClientesRecentes: false});
      this.error('Erro clientes Recentes', e);
    });
  }

  buscaServicosPendentes() {
    this.setState({loadingServicosPendentes: true});
    http('/servico/meus_pendentes',{
      method: 'GET'
    }).then((data) => {
      this.setState({loadingServicosPendentes: false, servicos: data})
    }).catch((e) => {
      this.setState({loadingServicosPendentes: false});
      this.error('Erro servicos pendentes', e);
    });
  }

  ref(card) {
    const elm = ReactDOM.findDOMNode(card);
    if(!elm) {
      return;
    }
    this.setState({
      width: elm.getBoundingClientRect().width-50
    })
  }

  componentDidMount() {
    documentTitle.set("Resumo");
    this.buscaClientesRecentes();
  }

  handleSaveService(servico) {
    if (servico) {
      http('/servico', {
        method: 'POST',
        body: servico,
      }).then(() => {
        Modal.success({
          title: 'Sucesso',
          content: 'Serviço salvo com sucesso',
        });
        this.buscaServicosPendentes();
      }).catch((err) => {
        Modal.error({
          title: 'Erro',
          content: err.message,
        });
      });
    }
  }

  render() {
    const {loadingClientesRecentes, loadingServicosPendentes, clientesRecentes, servicos} = this.state;

    return (
      <Screen>
        <Row gutter={16}>
          <Col lg={16}
            md={16}
            sm={24}
            xs={24}
            ref={this.ref}>
            <Loading tip="Carregando..."
              spinning={loadingServicosPendentes}>
              <Card title="Meus serviços"
                className={utilsCss.mt2}>
                {!servicos.length ? 
                  <Row className={[utilsCss.center, utilsCss.h3].join(' ')}>
                    Nenhum serviço pendente
                  </Row>
                : 
                  <Row className={utilsCss.mtn2}>
                    <Collapse bordered={false}>
                      {servicos.map((item, index) => {
                        return (
                          <Panel key={index}
                            header={
                              <Row type="flex"
                                align="middle">
                                <Col md={19}
                                  sm={24}
                                  xs={24}>
                                  <Row>
                                    <span className={[utilsCss.muted, utilsCss.mr2].join(' ')}>
                                      N° {item.codigo}
                                    </span> 
                                    <span className={utilsCss.h4}>
                                      {item.tipo}
                                    </span>
                                    <span className={utilsCss.ml2}>
                                      <small className={utilsCss.mr1}>MARCA:</small>
                                      {item.marca}
                                    </span>
                                  </Row>
                                  <Row>
                                    {item.status == 'P' ? 
                                      <span>
                                        <TagCustom color="#3498db"
                                          className={utilsCss.mr1}>
                                          Pendente
                                        </TagCustom>
                                        {item.inicio ?
                                          <TagCustom color="#3498db">
                                            <small className={utilsCss.mr1}>COMEÇOU:</small>
                                            <FormattedDate value={item.inicio}/>
                                          </TagCustom>
                                         : null}
                                      </span>
                                    : null}
                                    {item.status == 'I' ? 
                                      <span>
                                        <TagCustom color="#34495e"
                                          className={utilsCss.mr1}>
                                          Interrompido
                                        </TagCustom> 
                                        <TagCustom color="#34495e">
                                          <span className={utilsCss.mr2}>
                                            <small className={utilsCss.mr1}>COMEÇOU:</small>
                                            <FormattedDate value={item.inicio}/>
                                          </span>
                                          <span>
                                            <small className={utilsCss.mr1}>INTERROMPEU:</small>
                                            <FormattedDate value={item.fim}/>
                                          </span>
                                        </TagCustom>
                                      </span>
                                    : null}
                                  </Row>
                                  <Row>
                                    <span className={utilsCss.mr2}>
                                      <small className={utilsCss.mr1}>CLIENTE:</small>
                                      {item.cliente.nome}
                                    </span>
                                    <span className={utilsCss.mr2}>
                                      <small className={utilsCss.mr1}>CADASTRADO EM:</small>
                                      <FormattedDate value={item.cadastro}/>
                                    </span>
                                  </Row>
                                </Col>
                                <Col md={5}
                                  sm={14}
                                  xs={14}
                                  className={utilsCss.rightAlign}>
                                  {item.status == 'P' && item.inicio ? 
                                    <MotivoInterrompimento servico={item}
                                      onClose={this.buscaServicosPendentes}>
                                      <Tooltip title="Interromper">
                                        <a className={utilsCss.h2}>
                                          <Icon type="stop"/>
                                        </a>
                                      </Tooltip>
                                    </MotivoInterrompimento>
                                   : null}
                                  {item.status == 'P' && !item.inicio ? 
                                    <Popconfirm title="Tem certeza que deseja começar o serviço?" 
                                      onConfirm={() => 
                                        this.handleSaveService({
                                          ...item,
                                          inicio: new Date() 
                                        })
                                      }>
                                      <Tooltip title="Começar"
                                        onClick={(e) => e.stopPropagation()}>
                                        <a className={utilsCss.h2}>
                                          <Icon type="like"/>
                                        </a>
                                      </Tooltip>
                                    </Popconfirm>
                                   : null}
                                  {item.inicio ? 
                                    <Popconfirm title="Tem certeza que deseja finalizar o serviço?" 
                                      onConfirm={() => 
                                        this.handleSaveService({
                                          ...item,
                                          status: 'F',
                                          fim: new Date() 
                                        })
                                      }>
                                      <Tooltip title="Finalizar"
                                        onClick={(e) => e.stopPropagation()}>
                                        <a className={[utilsCss.h2, utilsCss.ml2].join(' ')}>
                                          <Icon type="check-circle"/>
                                        </a>
                                      </Tooltip>
                                    </Popconfirm>
                                   : null}
                                </Col>
                              </Row>
                            }>
                            <Row>
                              <b>Problema:</b>
                              <br/>
                              {item.problema}
                            </Row>
                            {item.status == 'I' && item.motivo ?
                              <Row className={utilsCss.mt2}>
                                <b>Motivo do interrompimento:</b>
                                <br/>
                                {item.motivo}
                              </Row>
                             : null}
                          </Panel>
                        )
                      })}
                    </Collapse>
                  </Row>
                }
              </Card>
            </Loading>
          </Col>
          <Col lg={8}
            md={8}
            sm={24}
            xs={24}>
            <Loading tip="Carregando..."
              spinning={loadingClientesRecentes}>
              <Card title="Clientes recentes"
                className={utilsCss.mt2}>
                {!clientesRecentes.length ? 
                  <Row className={[utilsCss.center, utilsCss.h3].join(' ')}>
                    Nenhum cliente cadastrado
                  </Row>
                : null}
                <Row className={utilsCss.mtn2}>
                  {clientesRecentes.map((item, index) => {
                    return (
                      <Row type="flex"
                        align="middle"
                        key={index}
                        className={clientesRecentes.length - 1 !== index ? 
                          [utilsCss.pb2, utilsCss.mb1, utilsCss.borderBottom].join(' ') : null
                        }>
                        <Row>
                          <Truncate>
                            {item.nome.toUpperCase()}
                          </Truncate>
                        </Row>
                        <Row>
                          <label className={utilsCss.mr1}>{item.pessoa == 'F' ? 'CPF' : 'CNPJ'}</label> 
                          <b>
                            {item.pessoa == 'F' ? <FormattedCPF value={item.documento}/> : <FormattedCNPJ value={item.documento}/>}
                          </b>
                          &nbsp; &nbsp; &nbsp; 
                          <label className={utilsCss.mr1}>Telefone</label> 
                          <b>{getFormatPhone(item.telefone)}</b>
                        </Row>
                      </Row>
                    )
                  })}
                </Row>
              </Card>
            </Loading>
          </Col>
        </Row>
      </Screen>
    );
  }
}

export default Resumo;
export {urlResumo};
