import React, { Component } from 'react';
import {Modal, Form, Button, Input, Row, Col, Checkbox} from 'antd';
import PropTypes from 'prop-types';
import http from 'consys/http';
import utilsCss from '../../css/utils.css';
import Loading from 'consys/Loading';
import ReactTimeout from 'react-timeout';

const FormItem = Form.Item;
const {Password} = Input;

class UsuarioCadastro extends Component {
  constructor() {
    super();
    this.state = {
      visible: false, 
      loading: true,
      ativo: true,
      admin: false
    };

    this.handleCancel = this.handleCancel.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.modal = this.modal.bind(this);
  }
  
  modal(visible) {
    const that = this;
    const {codigo, form} = this.props;
    this.setState({visible}, () => {
      setTimeout(()=>{this.inputFocus && this.inputFocus.focus()}, 1);
    });
    
    if (visible) {
      if (codigo) {
        http('/usuario', {
          method: 'GET',
          params: {codigo: codigo},
        }).then((data) => {
          const {admin, ativo, nome, login, senha} = data;
          that.setState({
            loading: false,
            admin: admin == 'S',
            ativo: ativo == 'S'
          });
          delete data.codigo;
          form.setFieldsValue({nome, login, senha});
        });
      }
      else {
        that.setState({loading: false, ativo: true, admin: false});
        form.resetFields();
      }
    }
  }
  
  handleSubmit(e) {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        this.setState({confirmLoading: true});
        const {onClose, codigo} = this.props;
        const {admin, ativo} = this.state;

        const obj = {
          codigo: codigo ? codigo : 0,
          admin: admin ? 'S' : 'N',
          ativo: ativo ? 'S' : 'N',
          ...values
        };

        http('/usuario', {
          method: 'POST',
          body: obj,
        }).then((res) => {
          Modal.success({
            title: 'Sucesso',
            content: codigo ? 'Atualizado com Sucesso' : 'Criado com Sucesso',
          });
          this.setState({
            visible: false,
            confirmLoading: false
          });
          onClose && onClose(res);
        }).catch((err) => {
          this.setState({confirmLoading: false});
          Modal.error({
            title: 'Erro',
            content: err.message,
          });
        });
      }
    });
  }

  handleCancel() {
    this.setState({visible: false});
    this.props.form.resetFields();
  }

  render() {
    const {getFieldDecorator} = this.props.form;
    const {confirmLoading, admin, ativo} = this.state;

    
    return (
      <span>
        <span onClick={() => { this.modal(true) }}
          className={utilsCss.pointer}>
          {this.props.children}
        </span>
        <Modal width="50%"
          centered
          visible={this.state.visible}
          destoyOnClose
          onOk={this.handleSubmit}
          onCancel={this.handleCancel}
          footer={[
            <Button key="back" 
              onClick={this.handleCancel}>
              Cancelar
            </Button>,
            <Button key="submit" 
              type="primary"
              onClick={this.handleSubmit}
              loading={confirmLoading}>
              Salvar
            </Button>
          ]}>
          <Loading tip="Carregando..."
            spinning={this.state.loading}>
            <Row className={[utilsCss.h2, utilsCss.bold].join(' ')}>
              Usuário
            </Row>
            <Row className={utilsCss.h4}>
              {this.props.codigo ? 'Editando usuário: '+this.props.codigo : 'Novo usuário'}
            </Row>
            <Row className={[utilsCss.h4, utilsCss.mb2].join(' ')}>
              Campos marcados com 
              <span style={{color: '#f5222d', fontFamily: 'SimSun', fontSize: '13px'}}>
                &nbsp;*&nbsp;
              </span>
              são obrigatórios
            </Row>
            <Form onSubmit={this.handleSubmit}>
              <Row>
                <FormItem label="Nome">
                  {getFieldDecorator('nome', {
                    rules: [{
                      required: true,
                      message: 'Nome é obrigatório, não pode ficar em branco'
                    }]
                  })(
                    <Input ref={(input) => this.inputFocus = input}/>
                  )}
                </FormItem>
              </Row>
              <Row gutter={16}>
                <Col md={12}
                  sm={24}
                  xs={24}>
                  <FormItem label="Login">
                    {getFieldDecorator('login', {
                      rules: [{
                        required: true,
                        message: 'Login é obrigatório, não pode ficar em branco'
                      }]
                    })(
                      <Input />
                    )}
                  </FormItem>
                </Col>
                <Col md={12}
                  sm={24}
                  xs={24}>
                  <FormItem label="Senha">
                    {getFieldDecorator('senha', {
                      rules: [{
                        required: true,
                        message: 'Senha é obrigatória, não pode ficar em branco'
                      }]
                    })(
                      <Password />
                    )}
                  </FormItem>
                </Col>
              </Row>
              <Row gutter={16}>
                <Col md={12}
                  sm={24}
                  xs={24}>
                  <Checkbox checked={ativo}
                    onChange={(e) => this.setState({ativo: e.target.checked})}>
                    Ativo
                  </Checkbox>
                </Col>
                <Col md={12}
                  sm={24}
                  xs={24}>
                  <Checkbox checked={admin}
                    onChange={(e) => this.setState({admin: e.target.checked})}>
                    Administrador
                  </Checkbox>
                </Col>
              </Row>
            </Form>
          </Loading>
        </Modal>
      </span>
    );
   }
}

UsuarioCadastro.propTypes = {
  onClose: PropTypes.func,
  form: PropTypes.object,
  codigo: PropTypes.number,
  children: PropTypes.node
};

const UsuarioCadastroForm = Form.create()(UsuarioCadastro);
const UsuarioCadastroTimeout = ReactTimeout(UsuarioCadastroForm);
export default UsuarioCadastroTimeout;