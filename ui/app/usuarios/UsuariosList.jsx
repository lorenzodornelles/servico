import React, { Component } from 'react';
import utilsCss from '../../css/utils.css';
import {Row, Input, Form, Col, Pagination, notification} from 'antd';
import documentTitle from 'consys/documentTitle';
import ReactTimeout from 'react-timeout';
import PropTypes from 'prop-types';
import Loading from 'consys/Loading';
import http from 'consys/http';
import Truncate from 'consys/Truncate';
import DeleteButton from 'consys/DeleteButton';
import dataUtils from 'consys/dataUtils';
import UsuarioCadastro from './UsuarioCadastro';
import EditButton from 'consys/EditButton';
import NewButton from 'consys/NewButton';
import TagCustom from '../tagCustom/TagCustom';

const FormItem = Form.Item;

const urlUsuarios = '/painel/usuarios';
class UsuariosList extends Component {
  constructor() {
    super();
    this.state = {
      data: [],
      loading: false,
      pagination: {pagina: 1, size: 10}
    };

    this.alteraFiltro = this.alteraFiltro.bind(this);
    this.fetch = this.fetch.bind(this);
  }

  componentDidMount() {
    documentTitle.set("Usuários");
    this.fetch();
  }

  alteraFiltro(input) {
    const filtro = input.target.value;
    if (this.searchBounce) {
      this.props.clearTimeout(this.searchBounce);
    }
    this.searchBounce = this.props.setTimeout(() => {
      this.fetch({descricao: filtro});
    }, 300);
  }

  fetch(params = {pagina: 1}) {
    this.setState({ loading: true });

    if (!params.pagina) {
      params = {...params, pagina: 1};
    }

    http('/usuario/consulta', {
      method: 'POST',
      body: params
    }).then((data) => {
      const {content, size, totalElements} = data;
      this.setState({
        loading: false,
        data: content,
        pagination: {
          pagina: params.pagina,
          size, 
          totalElements
        }
      });
    }).catch((data) => {
      notification.error({
        message: 'Erro!',
        description: data.message,
      });
    });
  }

  render() {
    const { data } = this.state;
    const { size, totalElements, pagina } = this.state.pagination;
    const {del, close} = this.props.dataUtils;  
    let usuarios = null;

    if (!data || !data.length){
      usuarios = (
        <Row style={{ width: '100%' }}
          className={utilsCss.p4}>
          <div className={[utilsCss.center, utilsCss.h4].join(' ')}>
            Nenhum resultado encontrado
          </div>
        </Row>
      );
    } else {
      usuarios = data.map(function(item, index){
        return(
          <Row key={index}
            type="flex"
            align="middle"
            className={[utilsCss.px2, utilsCss.py3, index > 0 ? utilsCss.borderTop : null].join(' ')}>
            <Col md={20}
              sm={24}
              xs={24}>
              <Row>
                <Truncate>
                  <span className={[utilsCss.bold, utilsCss.muted, utilsCss.mr2].join(' ')}>
                    {item.codigo}
                  </span> 
                  <span className={utilsCss.h3}>
                    {item.nome.toUpperCase()}
                  </span>
                </Truncate>
              </Row>
              <Row>
                {item.ativo == 'S' ? <TagCustom color="#2db7f5">Ativo</TagCustom> : <TagCustom color="#f50">Inativo</TagCustom>}
                {item.admin == 'S' ? <TagCustom color="#108ee9">Administrador</TagCustom> : null}
              </Row>
            </Col>
            <Col md={4}
              sm={24}
              xs={24}
              className={utilsCss.rightAlign}>
              <DeleteButton flat
                placement="topRight"
                onDelete={() =>  del(index, {
                  url: '/usuario',
                  method:'DELETE',
                  params: {codigo: item.codigo},
                  message: 'Usuário excluído com sucesso'
                })}/>
              <UsuarioCadastro codigo={item.codigo}
                onClose={(res) => close(res, index)}>
                <span className={utilsCss.ml3}>
                  <EditButton flat/>
                </span>
              </UsuarioCadastro>
            </Col>
          </Row>
        );
      });
    }

    return (
      <Row className={utilsCss.p2}>
        <Row className={[
            utilsCss.mt1, 
            utilsCss.mb1,
            utilsCss.pt1, 
            utilsCss.px3, 
            utilsCss.rounded, 
            utilsCss.border
          ].join(' ')}
          style={{background: '#fff'}}>
          <Col md={20}
            sm={24}
            xs={24}>
            <FormItem label="Pesquisar">
              <Input onChange={this.alteraFiltro}/>
            </FormItem>
          </Col>
          <Col md={4}
            sm={24}
            xs={24}
            className={utilsCss.rightAlign}>
            <FormItem colon={false}
              label=" ">
              <UsuarioCadastro onClose={() => this.fetch()}>
                <NewButton>Novo Usuário</NewButton>
              </UsuarioCadastro>
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Loading spinning={this.state.loading}
            tip="Carregando...">
            <Row style={{background: '#fff'}}
              className={[
                utilsCss.border, 
                utilsCss.px2, 
                utilsCss.rounded,
                utilsCss.mt2
              ].join(' ')}>
              {usuarios}
            </Row>
          </Loading>
          <Row className={[utilsCss.mt2, utilsCss.rightAlign].join(' ')}>
            <Pagination total={totalElements}
              onChange={this.fetch}
              showTotal={(total, range) => `${range[0]}-${range[1]} de ${total} items`}
              pageSize={size}
              current={pagina}/>
          </Row>
        </Row>
      </Row>
    );
  }
}

UsuariosList.propTypes = {
  setTimeout: PropTypes.func.isRequired,
  clearTimeout: PropTypes.func.isRequired,
  match: PropTypes.object,
  dataUtils: PropTypes.object
}

const UsuariosListDataUtils = dataUtils(UsuariosList);
const UsuariosListTimeout = ReactTimeout(UsuariosListDataUtils);
export default UsuariosListTimeout;
export {urlUsuarios};