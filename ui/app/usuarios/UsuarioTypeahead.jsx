import React, { Component } from 'react';
import Typeahead from 'consys/Typeahead';
import Lookup from 'consys/Lookup';
import PropTypes from 'prop-types';
import {Input, Form, Row} from 'antd';
import createFilter from 'consys/createFilter';
import ReactTimeout from 'react-timeout';

const FormItem = Form.Item;

class UsuarioTypeahead extends Component {
  constructor() {
    super();
    this.state = {};
    this.columns = [
      {
        title: 'Cod',
        dataIndex: 'codigo',
        sorter: true,
      },
      {
        title: 'Nome',
        dataIndex: 'nome',
        sorter: true
      }
    ];
    this.handleBusca = this.handleBusca.bind(this);
    this.handleSearchReady = this.handleSearchReady.bind(this);
  }
  handleBusca(params) {
    let nome = params.nome;

    if (this.searchBounce) {
      this.props.clearTimeout(this.searchBounce);
    }

    this.setState({loading: true, ...params});
    this.searchBounce = this.props.setTimeout(() => {
      const body = {
        filters: {
          ...createFilter('nome', nome)
        }
      }
      this.search(body);
    }, 900);
  }
  handleSearchReady(search) { 
    this.search = search;
  }
  render() {
    const {data} = this.state;
    const lookup = (
      <Lookup 
        body={
          <Row>
            <FormItem label="Nome">
              <Input onChange={(e) => this.handleBusca({nome: e.target.value})}/>
            </FormItem>
          </Row>
        }
        onSearchReady={this.handleSearchReady}
        data={data}
        title="Usuários"
        columns={this.columns} 
        url="/usuario/lookup"
        rowKey={item => item.codigo}
        width={800}
      />
    )
    return (
      <Typeahead {...this.props}
        title="Usuário"
        columns={this.columns} 
        url="/usuario/typeahead"
        lookup={lookup}
        view={item => item ? item.codigo+' - '+item.nome : null}/>
    );
  }
}

UsuarioTypeahead.propTypes = {
  onChange: PropTypes.func,
  setTimeout: PropTypes.func,
  clearTimeout: PropTypes.func
}

const UsuarioTypeaheadTimeout = ReactTimeout(UsuarioTypeahead);
export default UsuarioTypeaheadTimeout;