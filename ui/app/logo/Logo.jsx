import React, { Component } from 'react';
import logo from '../../img/logo.png';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import {urlResumo} from '../resumo/Resumo';

class Logo extends Component {
  render() {
    const {invert} = this.props;
    return (
      <Link to={urlResumo}>
        <img src={logo}/>
      </Link>
    );
  }
}

Logo.propTypes = {
  invert: PropTypes.bool,
};

export default Logo;