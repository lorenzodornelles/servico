import React, { Component } from 'react';
import {Row, Col} from 'antd';
import utilsCss from 'consys/utils.css';
import logo from '../../img/logo.png';
import {version} from '../../../package.json';

class Footer extends Component {
  render() {
    return (
      <Row>
        <Col sm={{span: 14, offset: 5}}
          xs={{span: 20, offset: 2}}
          className={[utilsCss.py3, utilsCss.mt3, utilsCss.muted, utilsCss.borderTop].join(' ')}>
          <Row className={utilsCss.center}>
            <img src={logo}
              width={50} 
              alt={<span>Serviço {new Date().getFullYear()}</span>}/>
            <br/>
          </Row>
          <Row className={[utilsCss.mt1, utilsCss.h6, utilsCss.center, utilsCss.muted].join(' ')}>
            <br/>
            versão {version}
          </Row>
        </Col>
      </Row>
    );
  }
}

export default Footer;