/*eslint-env node*/
const config = require('./webpack.config');
const webpack = require('webpack');
const WebpackShellPlugin = require('webpack-shell-plugin');

config.watch = false;
config.devtool = 'cheap-module-source-map';

Array.prototype.push.apply(config.plugins, 
  [
    new webpack.DefinePlugin({ 
      'process.env': {
        'NODE_ENV': JSON.stringify('production')
      }
    }),
    new webpack.optimize.UglifyJsPlugin({minimize: true}), 
    new webpack.optimize.AggressiveMergingPlugin(),
    new WebpackShellPlugin({
      onBuildStart: ['mvn clean'],
      onBuildEnd: ['mvn package']
    })
  ]
);

module.exports = config;